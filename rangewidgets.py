"""
    :Date: |date|
    :Revision: 1.0
    :Author: Franc SERRES <sefran007@gmail.com>
    :Description: This is a documention for python 3 qt5 range widgets
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.

    The ``rangewidgets`` module
    ===========================

    Use it to import range qt widget feature.

        .. image:: classes/classes_rangewidgets.png
            :alt: All classes
            :align: center
            :width: 800px
            :height: 1000px

    Example:

    .. code-block:: python

        from range import QSale


        start_range = 1
        end_range = 45
        min_range = 1
        max_range = 100

        myObject = QRangeProgressBar(start_range, end_range, min_range, max_range)

"""

# The short X.Y version. X = major incompatible features, Y = minor added features
__version__ = '2.0'
# The release tag. Tags:
#    'Pre-alpha' = feasibility,
#    'Alpha' = development,
#    'Beta' = testing,
#    'Release candidate' = qualification process,
#    'Stable release' = ready to deploy,
#    'Feature complete' = production ready,
#    'End of life' = obsolete
__release_life_cycle__ = 'Alpha'


# from math import log, nan, isnan, inf, isinf
from rangelevels import QScale

# from PyQt5 import QtCore
# from PyQt5.QtCore import QObject, Qt
# from PyQt5.QtWidgets import QWidget, QHBoxLayout, QSlider
# from PyQt5.QtGui import QPainter, QFont, QColor, QPen



class QBarRange(QScale):
    """
    .. image:: classes/QBarRange.png
        :alt: Diagram
        :align: center
        :height: 550px

    Bar range features objet.

    :param mindraw: value of minimal draw widget.
    :type mindraw: int
    :param maxdraw: value of maximal draw widget.
    :type maxdraw: int
    """

    # pylint: disable=R0902, R0914, R0915, R0904

    def __init__(self, mindraw, maxdraw, *args, **kwargs):
        """ Initialize class QProgress. """
        super().__init__(*args, **kwargs)
        # Initialisation parameters
        self.__minimum_draw_plage = mindraw
        self.__maximum_draw_plage = maxdraw
        self.__offset_minimum_bar = 0
        self.__offset_maximum_bar = 0
        self.__showoffset = None
        self.__show_range_draw = True

    def show_offset(self):
        """ Show offset set """
        self.__showoffset = True

    def hide_offset(self):
        """ Hide offset set """
        self.__showoffset = False

    def is_offset_show(self):
        """
        Get state show offset bar into plage.

        :Return: State of show offset bar.
        :rtype: bool
        """
        return self.__showoffset

    def show_range(self):
        """ Show range draw """
        self.__show_range_draw = True

    def hide_range(self):
        """ Hide range draw """
        self.__show_range_draw = False

    def is_range_show(self):
        """
        Get state show Range draw.

        :Return: State of show draw range.
        :rtype: bool
        """
        return self.__show_range_draw

    def set_offset_start_bar(self, offset=None):
        """
        Set start spacer for range progress bar.

        :param offset: value of start spacer for range progress bar.
        :type offset: int
        """
        self.__offset_minimum_bar = offset

    def get_offset_start_bar(self):
        """
        Set start spacer from range progress bar.

        :returns: Value of begining spacer bar.
        :rtype: int
        """
        return self.__offset_minimum_bar

    def get_draw_value_offset_start_bar(self):
        """
        Get real offset min draw value.

        :returns: Value of real draw begining spacer bar.
        :rtype: int
        """
        if self.__showoffset:
            if self.__offset_minimum_bar and self.__offset_minimum_bar > 0:
                offsetmin = self.__offset_minimum_bar
            else:
                offsetmin = 0
        else:
            offsetmin = 0
        return offsetmin

    def set_offset_end_bar(self, offset=None):
        """
        Set end spacer for range progress bar.

        :param offset: value of end spacer for range progress bar.
        :type offset: int
        """
        self.__offset_maximum_bar = offset

    def get_offset_end_bar(self):
        """
        Get end spacer from range progress bar.

        :returns: Value of ending spacer bar.
        :rtype: int
        """
        return self.__offset_maximum_bar

    def get_draw_value_offset_end_bar(self):
        """
        Get real offset max draw value.

        :returns: Value of real draw ending spacer bar.
        :rtype: int
        """
        if self.__showoffset:
            if self.__offset_maximum_bar and self.__offset_maximum_bar>0:
                offsetmax = self.__offset_maximum_bar
            else:
                offsetmax = 0
        else:
            offsetmax = 0
        return offsetmax

    def set_draw_value_minimum_plage(self, value):
        """
        Set value of minimum plage draw.

        :paran value: Value of minimum draw.
        :type value: int
        """
        self.__minimum_draw_plage = value

    def get_draw_value_minimum_plage(self):
        """
        Get value of minimimum value bar draw.

        :returns: Pixel value of Minimum index plage draw.
        :rtype: int
        """
        return self.__minimum_draw_plage

    def get_draw_value_start_bar(self):
        """
        Value of start draw bar.

        :returns: Pixel value of start bar.
        :rtype: int
        """
        if self.__showoffset:
            return self.get_draw_value_minimum_plage() + self.get_draw_value_offset_start_bar()
        return self.get_draw_value_minimum_plage()

    def set_draw_value_maximum_plage(self, value):
        """
        Set value of maximum index draw plage.

        :paran value: Value of maximum draw plage.
        :type value: int
        """
        self.__maximum_draw_plage = value

    def get_draw_value_maximum_plage(self):
        """
        Get value of maximum index draw plage.

        :returns: Pixel value of maximum index draw plage.
        :rtype: int
        """
        return self.__maximum_draw_plage

    def get_draw_value_end_bar(self):
        """
        Value of maximum bar draw.

        :returns: Pixel value of maximum bar.
        :rtype: int
        """
        if self.__showoffset:
            return self.get_draw_value_maximum_plage() - self.get_draw_value_offset_end_bar()
        return self.get_draw_value_maximum_plage()

    def get_draw_width_plage(self):
        """
        Value of pixel draw width plage.

        :returns: Pixel value of width draw plage.
        :rtype: int
        """
        return self.get_draw_value_maximum_plage() - self.get_draw_value_minimum_plage()

    def get_draw_width_bar(self):
        """
        Value of pixel bar width.
        (Need minimum and maximum plage set)

        :returns: Pixel value of width draw bar.
        :rtype: int
        """
        return self.get_draw_value_end_bar() - self.get_draw_value_start_bar()

    def get_draw_value_start_range(self):
        """
        Value of start all draw range.

        :returns: Real value of start all draw range.
        :rtype: int
        """
        minvalue = self.get_minimum()
        if minvalue is None:
            return None
        startvalue = self.get_start()
        if startvalue is None:
            return None
        endvalue = self.get_end()
        if endvalue is None:
            return None
        maxvalue = self.get_maximum()
        if maxvalue is None:
            return None

        if not self.is_range_show() or startvalue > endvalue:
            return None
        return int(self.get_draw_value(startvalue))

    def get_draw_value_end_range(self):
        """
        Value of end all draw range.

        :returns: Real value of end all draw range.
        :rtype: int
        """
        minvalue = self.get_minimum()
        if minvalue is None:
            return None
        startvalue = self.get_start()
        if startvalue is None:
            return None
        endvalue = self.get_end()
        if endvalue is None:
            return None
        maxvalue = self.get_maximum()
        if maxvalue is None:
            return None

        if not self.is_range_show() or startvalue > endvalue:
            return None
        return int(self.get_draw_value(endvalue))

    def get_draw_width_range(self):
        """
        Value pixel of width all draw range.

        :returns: Value width of all draw range.
        :rtype: int
        """
        startrange = self.get_draw_value_start_range()
        endrange = self.get_draw_value_end_range()
        if startrange is None or endrange is None:
            return None
        return endrange - startrange

    def get_draw_value(self, value):
        """
        Get draw value.

        :param value: value to scale draw range.
        :type value: int, float
        :returns: Real draw value.
        :rtype: int, float
        """
        return ((self.scale_value(value) -
                 self.get_scaled_value_minimum()) /
                 self.get_scaled_width_bar() *
                 self.get_draw_width_bar() +
                 self.get_draw_value_start_bar())

#    def get_draw_value_start_normal(self):
#        """
#        Value of start normal draw range.

#        :returns: Real value of start normal draw range.
#        :rtype: int
#        """
#        minvalue = self.get_minimum()
#        if minvalue is None:
#            return None
#        startvalue = self.get_start()
#        if startvalue is None:
#            return None
#        endvalue = self.get_end()
#        if endvalue is None:
#            return None
#        maxvalue = self.get_maximum()
#        if maxvalue is None:
#            return None

#        if not self.is_range_show() or startvalue > endvalue:
#            return None
#        else:
#            warningminvalue = self.get_minimum_warning()
#            if (not warningminvalue is None) and startvalue > warningminvalue:
#                if self.is_maximum_warning_show():
#                    if startvalue >= self.get_maximum_warning():
#                        return None
#                if self.is_maximum_alert_show():
#                    if startvalue >= self.get_maximum_alert():
#                        return None
#                if startvalue >= maxvalue:
#                    return int(self.get_draw_value(maxvalue))
#            else:
#                alertminvalue = self.get_minimum_alert()
#                if not (alertminvalue is None):
#                    if startvalue > alertminvalue:
#                        if (not warningminvalue is None) and self.is_minimum_warning_show():
#                            if endvalue > warningminvalue:
#                                return int(self.get_draw_value(warningminvalue))
#                            return None
#                    else:
#                        if (not warningminvalue is None) and self.is_minimum_warning_show():
#                            if endvalue > warningminvalue:
#                                return int(self.get_draw_value(warningminvalue))
#                            return None
#                        if self.is_minimum_alert_show():
#                            if endvalue > alertminvalue:
#                                return int(self.get_draw_value(alertminvalue))
#                            return None
#                else:
#                    if (not warningminvalue is None) and startvalue < warningminvalue:
#                        return int(self.get_draw_value(warningminvalue))
#            return int(self.get_draw_value(startvalue))


#    def get_draw_value_end_normal(self):
#        """
#        Value of end normal draw range.

#        :returns: Real value of end normal draw range.
#        :rtype: int
#        """
#        minvalue = self.get_minimum()
#        if minvalue is None:
#            return None
#        startvalue = self.get_start()
#        if startvalue is None:
#            return None
#        endvalue = self.get_end()
#        if endvalue is None:
#            return None
#        maxvalue = self.getScaledValueMaximum()
#        if maxvalue is None:
#            return None

#        if not self.is_range_show() or startvalue > endvalue:
#            return None
#        else:
#            warningmaxvalue = self.get_maximum_warning()
#            if (not warningmaxvalue is None) and endvalue < warningmaxvalue:
#                if self.is_minimum_warning_show():
#                    if endvalue <= self.get_minimum_warning():
#                        return None
#                if self.is_minimum_alert_show():
#                    if endvalue <= self.get_minimum_alert():
#                        return None
#                if endvalue <= minvalue:
#                    return int(self.get_draw_value(minvalue))
#            else:
#                alertmaxvalue = self.get_maximum_alert()
#                if not alertmaxvalue is None:
#                    if endvalue < alertmaxvalue:
#                        if self.is_maximum_warning_show():
#                            if startvalue < warningmaxvalue:
#                                return int(self.get_draw_value(warningmaxvalue))
#                            return None
#                    else:
#                        if  (not warningmaxvalue is None) and self.is_maximum_warning_show():
#                            if startvalue < warningmaxvalue:
#                                return int(self.get_draw_value(warningmaxvalue))
#                            return None
#                        if  self.is_maximum_alert_show():
#                            if startvalue < alertmaxvalue:
#                                return int(self.get_draw_value(alertmaxvalue))
#                            return None
#                else:
#                    if (not warningmaxvalue is None) and endvalue > warningmaxvalue:
#                        return int(self.get_draw_value(warningmaxvalue))
#            return int(self.get_draw_value(endvalue))


#    def get_draw_width_normal(self):
#        """
#        Value pixel of normal draw range.

#        :returns: Value width of normal draw range.
#        :rtype: int
#        """
#        startrange = self.get_draw_value_start_normal()
#        endrange = self.get_draw_value_end_normal()
#        if startrange is None or endrange is None:
#            return None
#        else:
#            return endrange - startrange


#    def get_draw_value_start_minimum_alert(self):
#        """
#        Value pixel of start minimum alert draw range.

#        :returns: Real value of start minimum alert draw range.
#        :rtype: int
#        """
#        if self.is_minimum_alert_show():
#            if self.is_minimum_alert_active():
#                startvalue = self.get_start()
#                minimimum = self.get_minimum()
#                if minimimum is None:
#                    return None
#                if startvalue > minimimum:
#                    return int(self.get_draw_value(startvalue))
#                else:
#                    return int(self.get_draw_value(minimimum))
#        return None


#    def get_draw_width_minimum_alert(self):
#        """
#        Value pixel of width minimum alert draw range.

#        :returns: Real value of width minimum alert draw range.
#        :rtype: int
#        """
#        alertminstart = self.get_draw_value_start_minimum_alert()
#        if not alertminstart is None:
#            alertminvalue = self.get_minimum_alert()
#            if alertminvalue is None:
#                return None
#            endvalue = self.get_end()
#            if endvalue is None:
#                return None
#            if endvalue < alertminvalue:
#                minimum = self.get_scaled_value_minimum()
#                if endvalue > minimum:
#                    return int(self.get_draw_value(endvalue) - alertminstart)
#                else:
#                    return int(self.get_draw_value(minimum) - alertminstart)
#            else:
#                return int(self.get_draw_value(alertminvalue) - alertminstart)
#        return None


#    def get_draw_value_start_minimum_warning(self):
#        """
#        Value pixel of start draw minimum warning draw range.

#        :returns: Real value of start minimum warning draw range.
#        :rtype: int
#        """
#        if self.is_minimum_warning_show():
#            if self.is_minimum_warning_active():
#                if self.is_minimum_alert_show():
#                    if self.is_minimum_alert_active():
#                        return int(self.get_draw_value(self.get_minimum_alert()))
#                return int(self.get_draw_value(self.get_start()))
#        return None


#    def get_draw_width_minimum_warning(self):
#        """
#        Value pixel of width minimum warning draw range.

#        :returns: Real value of minimum warning width draw range.
#        :rtype: int
#        """
#        warningminstart = self.get_draw_value_start_minimum_warning()
#        if not warningminstart is None:
#            endvalue = self.get_end()
#            warningmin = self.get_minimum_warning()
#            if endvalue > warningmin:
#                return int(self.get_draw_value(warningmin) - warningminstart)
#            else:
#                return int(self.get_draw_value(endvalue) - warningminstart)
#        return None


#    def get_draw_value_start_maximum_warning(self):
#        """
#        Value pixel of start maximum warning draw range.

#        :returns: Real value of start maximum warning draw range.
#        :rtype: int
#        """
#        if self.is_maximum_warning_show():
#            if self.is_maximum_warning_active():
#                startvalue = self.get_start()
#                warningmax = self.get_maximum_warning()
#                if startvalue < warningmax:
#                    return int(self.get_draw_value(warningmax))
#                else:
#                    return int(self.get_draw_value(startvalue))
#        return None


#    def get_draw_width_maximum_warning(self):
#        """
#        Value pixel of width maximum warning draw range.

#        :returns: Real value of maximum warning width draw range.
#        :rtype: int
#        """
#        warningmaxstart = self.get_draw_value_start_maximum_warning()
#        if not warningmaxstart is None:
#            if not self.get_draw_value_start_maximum_alert() is None:
#                return int(self.get_draw_value(self.get_maximum_alert()) - warningmaxstart)
#            return int(self.get_draw_value(self.get_end()) - warningmaxstart)
#        return None


#    def get_draw_value_start_maximum_alert(self):
#        """
#        Value pixel of start maximum alert draw range.

#        :returns: Real value of start maximum alert draw range.
#        :rtype: int
#        """
#        if self.is_maximum_alert_show():
#            if self.is_maximum_alert_active():
#                maximimum = self.get_maximum()
#                alertmax = self.get_maximum_alert()
#                startvalue = self.get_start()
#                if startvalue > alertmax:
#                    if startvalue < maximimum:
#                        return int(self.get_draw_value(startvalue))
#                    else:
#                        return int(self.get_draw_value(maximimum))
#                else:
#                    return int(self.get_draw_value(alertmax))
#        return None


#    def get_draw_width_maximum_alert(self):
#        """
#        Value pixel of width maximum alert draw range.

#        :returns: int -- initiatial default value None.
#        """
#        alertmaxstart = self.get_draw_value_start_maximum_alert()
#        if not alertmaxstart is None:
#            endvalue = self.get_end()
#            maximum = self.get_maximum()
#            if endvalue < maximum:
#                return int(self.get_draw_value(endvalue) - alertmaxstart)
#            else:
#                return int(self.get_draw_value(maximum) - alertmaxstart)
#        return None

    def update_progress(self, mindraw, maxdraw):
        """
        Update values of progress ranges.

        :param mindraw: value of minimal draw widget.
        :type mindraw: int
        :param maxdraw: value of maximal draw widget.
        :type maxdraw: int
        """
        self.set_draw_value_minimum_plage(mindraw)
        self.set_draw_value_maximum_plage(maxdraw)

        if self.get_draw_value_end_range() < self.get_draw_value_start_range():
            self.hide_range()
        else:
            self.show_range()
