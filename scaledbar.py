"""
    :Date: |date|
    :Revision: 2.0
    :Author: Franc SERRES <sefran007@gmail.com>
    :Description: This is a documention for python 3 bar slide api
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.
    :Release Life Cycle: Alpha

    The ``scaledbar`` module
    ===========================

    Use it get class progress levels bar features.

        .. image:: classes/classes_scaledbar.png
            :alt: All classes scaledbar.py
            :align: center
            :width: 800px
            :height: 1000px

    Example:

    .. code-block:: python

        from scaledbar import QScaledBar

        start_range = 1
        end_range = 45
        min_range = 1
        max_range = 100
        alertmin = 10
        warningmin = 30
        warningmax = 70
        alertmax = 90

        myObject = QScaledBar(min_range, max_range, start_range, end_range,
                          alertmin, warningmin, warningmax, alertmax,
                          islogscale=True)

"""

from math import isnan, isinf, inf, nan, log

class FactoryScales(type):
    """
    .. image:: classes/FactoryScales.png
        :alt: Diagram
        :align: center
        :height: 550px

    Create object with linear or logarithm features scales.

    :param linearfactor: Set scale factor.
    :type linearfactor: int, float
    :param logarithmbase: Set logarithm value base.
    :type logarithmbase: int
    :param setlinearscale: Choose linear scale system.
    :type setlinearscale: bool
    :param setlogarithmscale: Choose logarithm scale system.
    :type setlogarithmscale: bool
    :param setallscales: Choose all scales systems (linear and logarithm).
    :type setallscales: bool

    Example::

        from scaledbar import FactoryScales

        class Scale(FactoryScales, setlogarithmscale=True)
            ''' Scale class '''
            def __init__():
                ''' Initialize Scale class '''
                pass
        object = Scale()
    """
    # pylint: disable=C0303,W0613,E1120,E0102,R0912,R0914,R0915,W0212,C0204,W0201
    def __new__(cls, name, bases, namespace, **kwargs):
        """ Set class FactorySlide """
        setlinearscale = False
        setlogarithmscale = False
        if 'setlinearscale' in kwargs:
            if kwargs['setlinearscale']:
                setlinearscale = True
            del kwargs['setlinearscale']
        if 'setlogarithmscale' in kwargs:
            if kwargs['setlogarithmscale']:
                setlogarithmscale = True
            del kwargs['setlogarithmscale']
        if 'setallscales' in kwargs:
            if kwargs['setallscales']:
                setlinearscale = True
                setlogarithmscale = True
            del kwargs['setallscales']
        if 'linearfactor' in kwargs:
            if kwargs['linearfactor']:
                setlinearscale = True
        if 'logarithmbase' in kwargs:
            if kwargs['logarithmbase']:
                setlogarithmscale = True
        if 'log0' in kwargs:
            setlogarithmscale = True

        newclass = super(FactoryScales, cls).__new__(cls, name, bases, namespace)

        if setlinearscale:
            if 'linearfactor' in kwargs:
                setattr(newclass, '_linearfactor', kwargs['linearfactor'])
            else:
                setattr(newclass, '_linearfactor', None)
            def set_linear_factor(self, value=1):
                """
                Set linear factor.

                :param value: Linear factor value.
                :type value: int, float
                """
                self._linearfactor = value

            def get_linear_factor(self):
                """
                Get linear factor.

                :returns: Linear factor value.
                :rtype: int, float
                """
                return self._linearfactor

            setattr(newclass, set_linear_factor.__name__, set_linear_factor)
            setattr(newclass, get_linear_factor.__name__, get_linear_factor)

            if not setlogarithmscale:
                def scale_value(self, value):
                    """
                    Return value scaled.

                    If value is NaN, scale_value return None.

                    :param value: value to scale.
                    :type value: int, float, NaN, ±Infinity
                    :returns: Value scaled.
                    :rtype: int, float, NaN, ±Infinity
                    """
                    if isnan(value):
                        return None
                    if isinf(value):
                        return value
                    if isinstance(value, int):
                        return int(value * get_linear_factor(self))
                    return value * get_linear_factor(self)
                setattr(newclass, scale_value.__name__, scale_value)

        if setlogarithmscale:
            if 'logarithmbase' in kwargs:
                setattr(newclass, '_logarithmbase', kwargs['logarithmbase'])
            else:
                setattr(newclass, '_logarithmbase', None)
            if 'log0' in kwargs:
                setattr(newclass, '_log0', kwargs['log0'])
            else:
                setattr(newclass, '_log0', None)
            def set_logarithm_base(self, base=10):
                """
                Set logarithm base value.

                :param base: Base value of logrithm -- default value 10.
                :type base: int
                """
                if isinstance(base, int):
                    self._logarithmbase = base

            def get_logarithm_base(self):
                """
                Get default logarithm base.

                :returns: Value of base logarithm.
                :rtype: int
                """
                return self._logarithmbase

            def set_log0(self, value=None):
                """
                Set default value log for value 0

                value=None log(0) return Infinity.
                value=NaN log(0) return NaN.
                value=float log(0) return log(float)

                :param value: Value of log(0).
                :type value: None, NaN or float
                """
                if value is None or isnan(value) or isinstance(value, float):
                    self._log0 = value

            def get_log0(self):
                """
                Get default value log for value 0

                :returns: Value of 0 logarithm.
                :rtype: int
                """
                return self._log0

            setattr(newclass, set_logarithm_base.__name__, set_logarithm_base)
            setattr(newclass, get_logarithm_base.__name__, get_logarithm_base)
            setattr(newclass, set_log0.__name__, set_log0)
            setattr(newclass, get_log0.__name__, get_log0)
            
            if not setlinearscale:
                def scale_value(self, value):
                    """
                    Return signed log(abs(value))

                    If value is NaN, scale_value return None.

                    If get_log0() is None, value=0 return Infinity.
                    If get_log0() is NaN, value=0 return NaN.
                    If get_log0() is a float, value=0 return log(float)

                    :param value: value to scale.
                    :type value: int, float, NaN, ±Infinity
                    :returns: Value scaled.
                    :rtype: int, float, NaN, ±Infinity
                    """
                    if isnan(value):
                        return None

                    if value in (0, 0.0):
                        if self.get_log0() is None:
                            logvalue = -inf
                        elif isnan(get_log0(self)):
                            logvalue = nan
                        else:
                            if isinstance(value, int):
                                logvalue = int(log(get_log0(self), get_logarithm_base(self)))
                            else:
                                logvalue = log(get_log0(self), get_logarithm_base(self))
                    elif value > 0 or value > 0.0:
                        if isinf(value):
                            logvalue = inf
                        else:
                            if isinstance(value, int):
                                logvalue = int(log(value, get_logarithm_base(self)))
                            else:
                                logvalue = float(log(value, get_logarithm_base(self)))
                    else:
                        if isinf(value):
                            logvalue = inf
                        else:
                            if isinstance(value, int):
                                logvalue = -int(log(-value, get_logarithm_base(self)))
                            else:
                                logvalue = float(-log(-value, get_logarithm_base(self)))
                    return logvalue

                setattr(newclass, scale_value.__name__, scale_value)

        if setlinearscale and setlogarithmscale:
            setattr(newclass, '_linearscale', True)
            def set_linear_scale(self):
                """ Set linear scale step. """
                self._linearscale = True

            def set_logarithm_scale(self):
                """ Set logarithm scale step. """
                self._linearscale = False

            def is_linear_scale(self):
                """
                Get linear step state.

                :returns: State of linear scale.
                :rtype: bool
                """
                return self._linearscale

            def is_logarithm_scale(self):
                """
                Get logarithm step state.

                :returns: State of logarithm scale.
                :rtype: bool
                """
                return not self._linearscale

            def scale_value(self, value):
                """
                Return value scaled.

                If value is NaN, scale_value return None.

                for logarithm scale :

                  Return signed log(abs(value))

                  If get_log0() is None, value=0 return Infinity.
                  If get_log0() is NaN, value=0 return NaN.
                  If get_log0() is a float, value=0 return log(float)


                :param value: value to scale.
                :type value: int, float, NaN, ±Infinity
                :returns: Value scaled.
                :rtype: int, float, NaN, ±Infinity
                """
                if isnan(value):
                    return None

                if is_logarithm_scale(self):
                    if value in (0, 0.0):
                        if get_log0(self) is None:
                            logvalue = -inf
                        elif isnan(get_log0(self)):
                            logvalue = nan
                        else:
                            if isinstance(value, int):
                                logvalue = int(log(get_log0(self), get_logarithm_base(self)))
                            else:
                                logvalue = log(get_log0(self), get_logarithm_base(self))
                    elif value > 0 or value > 0.0:
                        if isinf(value):
                            logvalue = inf
                        else:
                            if isinstance(value, int):
                                logvalue = int(log(value, get_logarithm_base(self)))
                            else:
                                logvalue = float(log(value, get_logarithm_base(self)))
                    else:
                        if isinf(value):
                            logvalue = inf
                        else:
                            if isinstance(value, int):
                                logvalue = -int(log(-value, get_logarithm_base(self)))
                            else:
                                logvalue = float(-log(-value, get_logarithm_base(self)))
                    return logvalue

                if isinf(value):
                    return value
                if isinstance(value, int):
                    return int(value * get_linear_factor(self))
                return value * get_linear_factor(self)

            setattr(newclass, set_linear_scale.__name__, set_linear_scale)
            setattr(newclass, set_logarithm_scale.__name__, set_logarithm_scale)
            setattr(newclass, is_linear_scale.__name__, is_linear_scale)
            setattr(newclass, is_logarithm_scale.__name__, is_logarithm_scale)
            setattr(newclass, scale_value.__name__, scale_value)

        return newclass

    def __init__(cls, name, bases, namespace, **kwargs):
        ''' Init metaclass FactoryScales '''
        super().__init__(name, bases, namespace)



#    def __init__(self, linearfactor=1, islogscale=False, logarithmbase=10):
#        """ Initialize FactoryScales object """
#        super().__init__()
#        # Initialisation parameters
#        if islogscale:
#            self.set_logarithm_scale()
#        else:
#            self.set_linear_scale()
#        self.set_linear_factor(linearfactor)
#        self.set_logarithm_base(logarithmbase)
#        self.set_log0()


#class QScaledBar(QLevelsBar, FactoryScales):
#    """
#    .. image:: classes/QScaleBar.png
#        :alt: Diagram
#        :align: center
#        :height: 500px

#    Create a scaled range object.

#    Example::

#        from rangewidgets import QSale

#        scaledobject = QScaleBar(1,100,20,80,True,2)

#    :param minimum: The minimum bar value.
#    :type minimum: int
#    :param maximum: The maximum bar value.
#    :type maximum: int
#    :param start: The start of range.
#    :type start: int
#    :param end: The end of range.
#    :type end: int
#    :param alert_min: Level of minimum alert value.
#    :type alert_min: int
#    :param warning_min: Level of minimum warning value.
#    :type warning_min: int
#    :param warning_max: Level of maximum warning value.
#    :type  warning_max: int
#    :param alert_max: Level of maximum alert value.
#    :type alert_max: int
#    :param islogscale: Choose logarithm state scale.
#    :type islogscale: bool
#    :param logarithmbase: Choose logarithm value base.
#    :type logarithmbase: int
#    """

#    def __init__(self, *args, **kwargs):
#        """ Initialize class QScaleBar. """
#        super().__init__(*args, **kwargs)
#        if hasattr(self, "get_minimum_alert"):
#            def get_scaled_value_minimum_alert():
#                """
#                Return minimum alert value scaled.

#                :returns:Value scaled of minimum alert.
#                :rtype: int, float
#                """
#                if not self.get_minimum_alert() is None:
#                    if self.is_logarithm_scale():
#                        if self.get_minimum_alert() == 0:
#                            logalert_min = log(1, self.get_logarithm_base())
#                        elif self.get_minimum_alert() > 0:
#                            logalert_min = log(self.get_minimum_alert(), self.get_logarithm_base())
#                        elif self.get_minimum_alert() < 0:
#                            logalert_min = -log(-float(self.get_minimum_alert()),
#                                                self.get_logarithm_base())
#                        return logalert_min
#                    return self.get_minimum_alert() * self.get_linear_factor()
#                return None

#            self.get_scaled_value_minimum_alert = get_scaled_value_minimum_alert

#        if hasattr(self, "get_minimum_warning"):
#            def get_scaled_value_minimum_warning():
#                """
#                Return minimum warning value scaled.

#                :returns: Value scaled of minimum warning.
#                :rtype: int, float
#                """
#                if not self.get_minimum_warning() is None:
#                    if self.is_logarithm_scale():
#                        if self.get_minimum_warning() == 0:
#                            logwarning_min = log(1, self.get_logarithm_base())
#                        elif self.get_minimum_warning() > 0:
#                            logwarning_min = log(self.get_minimum_warning(),
#                                                 self.get_logarithm_base())
#                        elif self.get_minimum_warning() < 0:
#                            logwarning_min = -log(-float(self.get_minimum_warning()),
#                                                  self.get_logarithm_base())
#                        return logwarning_min
#                    return self.get_minimum_warning() * self.get_linear_factor()
#                return None

#            self.get_scaled_value_minimum_warning = get_scaled_value_minimum_warning

#        if hasattr(self, "get_maximum_warning"):
#            def get_scaled_value_maximum_warning(self):
#                """
#                Return maximum warning value scaled.

#                :returns: Value scaled of maximum warning.
#                :rtype: int, float
#                """
#                if not self.get_maximum_warning() is None:
#                    if self.is_logarithm_scale():
#                        if self.get_maximum_warning() == 0:
#                            logwarning_max =  log(1, self.get_logarithm_base())
#                        if self.get_maximum_warning() > 0:
#                            logwarning_max =  log(self.get_maximum_warning(),
#                                                  self.get_logarithm_base())
#                        if self.get_maximum_warning() < 0:
#                            logwarning_max =  -log(-self.get_maximum_warning(),
#                                                   self.get_logarithm_base())
#                        return logwarning_max
#                    return self.get_maximum_warning() * self.get_linear_factor()
#                return None
#            self.get_scaled_value_maximum_warning = get_scaled_value_maximum_warning

#        if hasattr(self, "get_maximum_alert"):
#            def get_scaled_value_maximum_alert():
#                """
#                Return maximum alert value scaled.

#                :returns: Value scaled of maximum alert.
#                :rtype: int, float
#                """
#                if not self.get_maximum_alert() is None:
#                    if self.is_logarithm_scale():
#                        if self.get_maximum_alert() == 0:
#                            logalert_max = log(1, self.get_logarithm_base())
#                        if self.get_maximum_alert() > 0:
#                            logalert_max = log(self.get_maximum_alert(), self.get_logarithm_base())
#                        if self.get_maximum_alert() < 0:
#                            logalert_max = -log(-float(self.get_maximum_alert()),
#                                                self.get_logarithm_base())
#                        return logalert_max
#                    return self.get_maximum_alert() * self.get_linear_factor()
#                return None
#            self.get_scaled_value_maximum_alert = get_scaled_value_maximum_alert

#    def get_scaled_value_minimum(self):
#        """
#        Return minimum bar value scaled.

#        :returns: Value scaled of minimum bar.
#        :rtype: int, float
#        """
#        if self.is_logarithm_scale():
#            # Set value minimum progress bar
#            if self.get_minimum() == 0:
#                if self.get_log0() is None:
#                    return -inf
#                if isnan(self.get_log0()):
#                    return nan
#                logvalue = log(self.get_log0(), self.get_logarithm_base())
#            elif self.get_minimum() > 0:
#                logvalue = log(self.get_minimum(), self.get_logarithm_base())
#            else:
#                logvalue = -log(-self.get_minimum(), self.get_logarithm_base())
#            return logvalue
#        return self.get_minimum() * self.get_linear_factor()

#    def get_scaled_value_maximum(self):
#        """
#        Return maximum bar value scaled.

#        :returns: Value scaled of maximum bar.
#        :rtype: int, float
#        """
#        if self.is_logarithm_scale():
#            # Set value maximum progress bar
#            if self.get_maximum() == 0:
#                if self.get_log0() is None:
#                    return -inf
#                if isnan(self.get_log0()):
#                    return nan
#                logvalue = log(self.get_log0(), self.get_logarithm_base())
#            elif self.get_maximum() > 0:
#                logvalue = log(self.get_maximum(), self.get_logarithm_base())
#            else:
#                logvalue = -log(-self.get_maximum(), self.get_logarithm_base())
#            return logvalue
#        return self.get_maximum() * self.get_linear_factor()

#    def get_scaled_width_bar(self):
#        """
#        Return width bar value scaled.

#        :returns: Value scaled of width bar.
#        :rtype: int, float
#        """
#        if self.is_logarithm_scale():
#            return log(self.get_maximum() - self.get_minimum(), self.get_logarithm_base())
#        return (self.get_maximum() - self.get_minimum()) * self.get_linear_factor()

#    def get_scaled_value_start(self):
#        """
#        Return start range valulogvalueende scaled.

#        :returns: Value scaled of start range.
#        :rtype: int, float
#        """
#        if self.is_logarithm_scale():
#            #Set value start range
#            if self.get_start() == 0:
#                if self.get_log0() is None:
#                    return -inf
#                if isnan(self.get_log0()):
#                    return nan
#                logvalue = log(self.get_log0(), self.get_logarithm_base())
#            elif self.get_start() > 0:
#                logvalue = log(self.get_start(), self.get_logarithm_base())
#            else:
#                logvalue = -log(-self.get_start(), self.get_logarithm_base())
#            return logvalue
#        return self.get_start() * self.get_linear_factor()

#    def get_scaled_value_end(self):
#        """
#        Return end range value scaled.

#        :returns: Value scaled of end range.
#        :rtype: int, float
#        """
#        if self.is_logarithm_scale():
#            # Set value end range
#            if self.get_end() == 0:
#                if self.get_log0() is None:
#                    return -inf
#                if isnan(self.get_log0()):
#                    return nan
#                logvalue = log(self.get_log0(), self.get_logarithm_base())
#            elif self.get_end() > 0:
#                logvalue = log(self.get_end(), self.get_logarithm_base())
#            else:
#                logvalue = -log(-self.get_end(), self.get_logarithm_base())
#            return logvalue
#        return self.get_end() * self.get_linear_factor()

#    def get_scaled_width_range(self):
#        """
#        Return width range value scaled.

#        :returns: Value scaled of width range.
#        :rtype: int, float
#        """
#        if self.is_logarithm_scale():
#            return log(self.get_end() - self.get_start(), self.get_logarithm_base())
#        return (self.get_end() - self.get_start()) * self.get_linear_factor()
