<!-- Qt5 Range widgets documentation master file, created by
sphinx-quickstart on Thu Mar 21 13:01:54 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
Welcome to Qt5 Range widgets documentation!

# Range widgets


* **mod**

    rangewidgets



* **Date**

    2019-07-11



* **Revision**

    1.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)>



* **Description**

    This is a documention for python 3 qt5 range widgets



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.


## The `rangewidgets` module

Use it to import range qt widget feature.

> 

> ![image](docs/source/classes/classes.png)

Example:

```
from rangewidgets import QRangeProgressBar

start_range = 1
end_range = 45
min_range = 1
max_range = 100

myObject = QRangeProgressBar(start_range, end_range, min_range, max_range)
```

## Range Class


#### class rangewidgets.QRange(minimum, maximum, start, end)


![image](docs/source/classes/QRange.png)

Create a range object.

Example:

```
from rangewidgets import QRange

rangeobject = QRange(1,100,20,80)
print('Minimum: %s' % rangeobject.getMinimum())
print('Start: %s' % rangeobject.getStart())
print('End: %s' % rangeobject.getEnd())
print('Maximum: %s' % rangeobject.getMaximum())
```


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The minimum bar value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The maximum bar value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The end of range.



#### getEnd()
Get end range value.


* **Returns**

    Value of end range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getMaximum()
Get maximum bar value.


* **Returns**

    Value of maximum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getMinimum()
Get minimum bar value.


* **Returns**

    Value of minimum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getStart()
Get start range value.


* **Returns**

    Value of start range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### setEnd(value)
Set end range value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – End range value – default value 80.



#### setMaximum(value)
Set maximum bar value


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Maximum bar value – default value 100.



#### setMinimum(value)
Set minimum bar value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum bar value – default value 0.



#### setStart(value)
Set start range value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.



#### class rangewidgets.QLevels(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max)


![image](docs/source/classes/QLevels.png)

Create a range object with levels informations.

Example:

```
from rangewidgets import QLevels

rangeobject = QLevels(1,100,20,80,10,30,70,90)
print('Minimum: %s' % rangeobject.getMinimum())
print('Minimum alert end: %s' % rangeobject.getMinimumAlert())
print('Minimum warning end: %s' % rangeobject.getMinimumWarning())
print('Start: %s' % rangeobject.getStart())
print('End: %s' % rangeobject.getEnd())
print('Maximum warning start: %s' % rangeobject.getMaximumWarning())
print('Maximum alert start: %s' % rangeobject.getMaximumAlert())
print('Maximum: %s' % rangeobject.getMaximum())
```


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum bar value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum bar value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimuScalem alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.



#### getMaximumAlert()
Get start maximum alert level value.


* **Returns**

    Maximum start alert level.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getMaximumWarning()
Get start maximum warning level value.


* **Returns**

    Maximum start warning level.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getMinimumAlert()
Get end minimum alert level value.


* **Returns**

    Minimum end alert level.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getMinimumWarning()
Get end warning minimum level value.


* **Returns**

    Minimum end level warning.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### setMaximumAlert(value)
Set start maximum alert level value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Maximum start alert level.



#### setMaximumWarning(value)
Set start maximum warning level value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Maximum start warning level.



#### setMinimumAlert(value)
Set end minimum alert level value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end alert level.



#### setMinimumWarning(value)
Set end minimum warning level value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end warning level.



#### class rangewidgets.QFactoryRange()
Create a general range object.

Example:

```
from rangewidgets import QFactoryRange

rangeobject = QFactoryRange(1,100,20,80,True,10,30,70,90)
```


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum bar value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum bar value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **islevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set levels adding.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.



* **Returns**

    A range with levels=False.



* **Return type**

    QRange



* **Returns**

    A range with levels informations with levels=True.



* **Return type**

    QLevels


## Scale Class


#### class rangewidgets.Scale(islogscale, logarithmbase=10)


![image](docs/source/classes/Scale.png)

Create object with linear or logarithm features scales.


* **Parameters**

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### getLinearFactor()
Get linear factor.


* **Returns**

    Linear factor value.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getLog0()
Get default value log for value 0


* **Returns**

    Value of 0 logarithm.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getLogarithmBase()
Get default logarithm base.


* **Returns**

    Value of base logarithm.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### isLogarithmScale()
Get logarithm step state.


* **Returns**

    State of logarithm scale.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### scaleValue(value)
Return value scaled.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*,*[*float*](https://docs.python.org/3/library/functions.html#float)) – value to scale.



* **Returns**

    Value scaled.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### setLinearFactor(value=1)
Set linear factor.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Linear factor value.



#### setLinearScale()
Set linear scale step.


#### setLog0(value=0.1)
Set default value log for value 0


* **Parameters**

    **value** ([*float*](https://docs.python.org/3/library/functions.html#float)) – Value of 0.



#### setLogarithmBase(base=10)
Set logarithm base value.


* **Parameters**

    **base** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Base value of logrithm – default value 10.



#### setLogarithmScale()
Set logarithm scale step.


#### class rangewidgets.QScale(minimum, maximum, start, end, islogscale, logarithmbase=10)


![image](docs/source/classes/QScale.png)

Create a scaled range object.

Example:

```
from rangewidgets import QSale

scaledobject = QScale(1,100,20,80,True,2)
```


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum bar value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum bar value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### getScaledValueEnd()
Return end range value scaled.


* **Returns**

    Value scaled of end range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMaximum()
Return maximum bar value scaled.


* **Returns**

    Value scaled of maximum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMinimum()
Return minimum bar value scaled.


* **Returns**

    Value scaled of minimum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueStart()
Return start range valulogvalueende scaled.


* **Returns**

    Value scaled of start range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledWidthBar()
Return width bar value scaled.


* **Returns**

    Value scaled of width bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledWidthRange()
Return width range value scaled.


* **Returns**

    Value scaled of width range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### class rangewidgets.QScaleLevels(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase=10)


![image](docs/source/classes/QScaleLevels.png)

Create a scaled range object with levels informations.

Example:

```
from rangewidgets import QSaleLevels

scaledobject = QScaleLevels(1,100,20,80,10,30,70,90,True,2)
```


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum bar value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum bar value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### getScaledValueEnd()
Return end range value scaled.


* **Returns**

    Value scaled of end range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMaximum()
Return maximum bar value scaled.


* **Returns**

    Value scaled of maximum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMaximumAlert()
Return maximum alert value scaled.


* **Returns**

    Value scaled of maximum alert.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMaximumWarning()
Return maximum warning value scaled.


* **Returns**

    Value scaled of maximum warning.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMinimum()
Return minimum bar value scaled.


* **Returns**

    Value scaled of minimum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueMinimumAlert()
Return minimum alert value scaled.

:returns:Value scaled of minimum alert.
:rtype: int, float


#### getScaledValueMinimumWarning()
Return minimum warning value scaled.


* **Returns**

    Value scaled of minimum warning.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledValueStart()
Return start range valulogvalueende scaled.


* **Returns**

    Value scaled of start range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledWidthBar()
Return width bar value scaled.


* **Returns**

    Value scaled of width bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getScaledWidthRange()
Return width range value scaled.


* **Returns**

    Value scaled of width range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### class rangewidgets.QFactoryScale()
Create a generic scaled range.

Example:

```
from rangewidgets import QSaleLevels

scaledobject = QScaleLevels(1,100,20,80,True,2,True,10,30,70,90)
```


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum bar value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum bar value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.

    * **levels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose levels adding.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.



* **Returns**

    A scaled range if levels=False.



* **Return type**

    QScale or QScaleLevels



* **Returns**

    A scaled range with levels informations if levels=True.



* **Return type**

    QScaleLevels


## Progress Class


#### class rangewidgets.BarRange(mindraw, maxdraw)


![image](docs/source/classes/BarRange.png)

Bar range features objet.


* **Parameters**

    * **mindraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of minimal draw widget.

    * **maxdraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of maximal draw widget.



#### getDrawValue(value)
Get draw value.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – value to scale draw range.



* **Returns**

    Real draw value.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### getDrawValueEndBar()
Value of maximum bar draw.


* **Returns**

    Pixel value of maximum bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueEndRange()
Value of end all draw range.


* **Returns**

    Real value of end all draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueMaximumPlage()
Get value of maximum index draw plage.


* **Returns**

    Pixel value of maximum index draw plage.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueMinimumPlage()
Get value of minimimum value bar draw.


* **Returns**

    Pixel value of Minimum index plage draw.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueOffsetEndBar()
Get real offset max draw value.


* **Returns**

    Value of real draw ending spacer bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueOffsetStartBar()
Get real offset min draw value.


* **Returns**

    Value of real draw begining spacer bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartBar()
Value of start draw bar.


* **Returns**

    Pixel value of start bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartRange()
Value of start all draw range.


* **Returns**

    Real value of start all draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthBar()
Value of pixel bar width.
(Need minimum and maximum plage set)


* **Returns**

    Pixel value of width draw bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthPlage()
Value of pixel draw width plage.


* **Returns**

    Pixel value of width draw plage.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthRange()
Value pixel of width all draw range.


* **Returns**

    Value width of all draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getOffsetEndBar()
Get end spacer from range progress bar.


* **Returns**

    Value of ending spacer bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getOffsetStartBar()
Set start spacer from range progress bar.


* **Returns**

    Value of begining spacer bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### hideOffset()
Hide offset set


#### hideRange()
Hide range draw


#### isOffsetShow()
Get state show offset bar into plage.


* **Return**

    State of show offset bar.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isRangeShow()
Get state show Range draw.


* **Return**

    State of show draw range.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### setDrawValueMaximumPlage(value)
Set value of maximum index draw plage.


* **Paran value**

    Value of maximum draw plage.



#### setDrawValueMinimumPlage(value)
Set value of minimum plage draw.


* **Paran value**

    Value of minimum draw.



#### setOffsetEndBar(offset=None)
Set end spacer for range progress bar.


* **Parameters**

    **offset** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of end spacer for range progress bar.



#### setOffsetStartBar(offset=None)
Set start spacer for range progress bar.


* **Parameters**

    **offset** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of start spacer for range progress bar.



#### showOffset()
Show offset set


#### showRange()
Show range draw


#### class rangewidgets.QProgress(mindraw, maxdraw, minimum, maximum, start, end, islogscale, logarithmbase=10)


![image](docs/source/classes/QProgress.png)

Create a progress range object

Example:

```
from rangewidgets import QProgress

progressobject = QProgress(0, 400,-100,100,-50,50,True,2)
```


* **Parameters**

    * **mindraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum draw widget value.

    * **maxdraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum draw widget value.

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum plage value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum plage value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### updateProgress(mindraw, maxdraw)
Update values of progress ranges.


* **Parameters**

    * **mindraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of minimal draw widget.

    * **maxdraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of maximal draw widget.



#### class rangewidgets.QProgressLevels(mindraw, maxdraw, minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


![image](docs/source/classes/QProgressLevels.png)

Create a progress range object with levels values.

Example:

```
from rangewidgets import QProgressLevels

progressobject = QProgressLevels(0, 400,-100,100,-50,50,-90,-70,70,90,True,2)
```


* **Parameters**

    * **mindraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum draw widget value.

    * **maxdraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum draw widget value.

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum plage value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum plage value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### getDrawValueEndNormal()
Value of end normal draw range.


* **Returns**

    Real value of end normal draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartMaximumAlert()
Value pixel of start maximum alert draw range.


* **Returns**

    Real value of start maximum alert draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartMaximumWarning()
Value pixel of start maximum warning draw range.


* **Returns**

    Real value of start maximum warning draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartMinimumAlert()
Value pixel of start minimum alert draw range.


* **Returns**

    Real value of start minimum alert draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartMinimumWarning()
Value pixel of start draw minimum warning draw range.


* **Returns**

    Real value of start minimum warning draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawValueStartNormal()
Value of start normal draw range.


* **Returns**

    Real value of start normal draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthMaximumAlert()
Value pixel of width maximum alert draw range.


* **Returns**

    int – initiatial default value None.



#### getDrawWidthMaximumWarning()
Value pixel of width maximum warning draw range.


* **Returns**

    Real value of maximum warning width draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthMinimumAlert()
Value pixel of width minimum alert draw range.


* **Returns**

    Real value of width minimum alert draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthMinimumWarning()
Value pixel of width minimum warning draw range.


* **Returns**

    Real value of minimum warning width draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getDrawWidthNormal()
Value pixel of normal draw range.


* **Returns**

    Value width of normal draw range.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### hideAlerts()
Hide alerts levels


#### hideLevels()
Hide levels bar indicators


#### hideMaximumAlert()
Hide maximum alert


#### hideMaximumWarning()
Hide maximum warning level


#### hideMaximumsLevels()
Hide maximums levels


#### hideMinimumAlert()
Hide minimum alert


#### hideMinimumWarning()
Hide minimum warning level


#### hideMinimumsLevels()
Hide minimums levels


#### hideWarnings()
Hide warnings levels


#### isAlertsShow()
State alerts levels show


#### isLevelsShow()
State of levels show.


* **Returns**

    State levels show from progress bar.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMaximumAlertActive()
State of maximum alert show


* **Returns**

    State of Maximum alert show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMaximumAlertShow()
Set of maximum alert show


* **Returns**

    Set of Maximum alert show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMaximumWarningActive()
State of maximum warning show


* **Returns**

    State of Maximum warning show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMaximumWarningShow()
Set of maximum warning show


* **Returns**

    Set of Maximum warning show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMaximumsLevelsShow()
State maximum levels show


#### isMinimumAlertActive()
State of minimum alert show


* **Returns**

    State of Minimum alert show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMinimumAlertShow()
Set of minimum alert show


* **Returns**

    Set of Minimum alert show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMinimumWarningActive()
State of minimum warning show


* **Returns**

    State of Minimum warning show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMinimumWarningShow()
Set of minimum warning show


* **Returns**

    Set of Minimum warning show



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMinimumsLevelsShow()
State minimum levels show


#### isWarningsShow()
State warnings levels show


#### showAlerts()
Show alerts levels


#### showLevels()
Show levels bar indicators


#### showMaximumAlert()
Show maximum alert


#### showMaximumWarning()
Show maximum warning levela


#### showMaximumsLevels()
Show maximums levels


#### showMinimumAlert()
Show minimum alert


#### showMinimumWarning()
Show minimum warning level


#### showMinimumsLevels()
Show minimums levels


#### showWarnings()
Show warnings levels


#### updateProgress(mindraw, maxdraw)
Update values of progress ranges and levels.


* **Parameters**

    * **mindraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of minimal draw widget.

    * **maxdraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – value of maximal draw widget.



#### class rangewidgets.QFactoryProgress()
Create a generic progress range.

Example:

```
from rangewidgets import QFactoryProgress

progressobject = QFactoryProgress(0, 400,-100,100,-50,50,True,2,True,-90,-70,70,90)
```


* **Parameters**

    * **mindraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum draw widget value.

    * **maxdraw** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum draw widget value.

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The minimum plage value.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The maximum plage value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.

    * **levels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose levels adding.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.



* **Returns**

    A range progress if levels=False.



* **Return type**

    QProgress



* **Returns**

    A range progress with levels informations if levels=True.



* **Return type**

    QProgressLevels


## Range Progress Bar Class



![image](pictures/QRangeProgressBar.png)


#### class rangewidgets.TextsBar()


![image](docs/source/classes/TextsBar.png)

Texts extremums bar features object.


#### getExtremumTextsFont()

* **Returns**

    Get text extremum font.



* **Return type**

    QFont



#### getExtremumTextsFontSize()

* **Returns**

    Get text extremum font size.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getMaximumText()

* **Returns**

    Get fixed text of minimum progress bar.



* **Return type**

    string



#### getMaximumTextColor()


![image](pictures/QRangeProgressBar_ColorTextMaximum.png)


* **Returns**

    Get color Text maximum plage.



* **Return type**

    QColor



#### getMinimumText()

* **Returns**

    Get fixed text of minimum progress bar.



* **Return type**

    string



#### getMinimumTextColor()


![image](pictures/QRangeProgressBar_ColorTextMinimum.png)


* **Returns**

    Get color Text minimum plage.



* **Return type**

    QColor



#### hideExtremumsTexts()


![image](pictures/QRangeProgressBar_HideRangeExtremums.png)

Hide text of maximum/minimum progress bar.



![image](pictures/QRangeProgressBar_HideExtremumsTexts.png)

or with showRangeTexts():


#### isExtremumsTextsShow()

* **Returns**

    State maximum/minimum labels show from progress bar.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMaximumLabelReadingReverse()


![image](pictures/QRangeProgressBar_LabelMaxReadingReverse.png)


* **Returns**

    State of reverse reading direction for label maximum.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isMinimumLabelReadingReverse()


![image](pictures/QRangeProgressBar_LabelMinReadingReverse.png)


* **Returns**

    State of reverse reading direction for label minimum.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### setExtremumTextsFont(font=<PyQt5.QtGui.QFont object>)
Set text extremum font.


* **Parameters**

    **font** (*QFont*) – font object of minimum and maximum text.



#### setExtremumTextsFontSize(size=10)
Set text extremum font size.


* **Parameters**

    **size** ([*int*](https://docs.python.org/3/library/functions.html#int)) – size of minimum and maximum text.



#### setMaximumLabelPositionBottom()


![image](pictures/QRangeProgressBar_LabelMaxPositionBottom.png)

Set label maximum progress bar position bottom.


#### setMaximumLabelPositionCenter()


![image](pictures/QRangeProgressBar_LabelMaxPositionCenter.png)

Set label maximum progress bar position center.


#### setMaximumLabelPositionTop()


![image](pictures/QRangeProgressBar_LabelMaxPositionTop.png)

Set label maximum progress bar position top.


#### setMaximumLabelReadingReverse(revert=False)


![image](pictures/QRangeProgressBar_LabelMaxReadingReverse.png)

Reverse the reading direction of label maximum.


* **Parameters**

    **revert** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – state of the inversion of the reading direction.



#### setMaximumText(text=None)
Set fixed text for minimum progress bar.


* **Parameters**

    **text** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – Text of maximum progress bar.



#### setMaximumTextColor(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorTextMaximum.png)

Set color Text maximum progress bar.


* **Parameters**

    **color** (*QColor*) – color object of maximum progress bar text.



#### setMinimumLabelPositionBottom()


![image](pictures/QRangeProgressBar_LabelMinPositionBottom.png)

Set label minimum progress bar position bottom.


#### setMinimumLabelPositionCenter()


![image](pictures/QRangeProgressBar_LabelMinPositionCenter.png)

Set label minimum progress bar position center.


#### setMinimumLabelPositionTop()


![image](pictures/QRangeProgressBar_LabelMinPositionTop.png)

Set label minimum progress bar position top.


#### setMinimumLabelReadingReverse(revert=False)


![image](pictures/QRangeProgressBar_LabelMinReadingReverse.png)

Reverse the reading direction of label minimum.


* **Parameters**

    **revert** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – state of the inversion of the reading direction.



#### setMinimumText(text=None)
Set fixed text for minimum progress bar.


* **Parameters**

    **text** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – Text of minimum progress bar.



#### setMinimumTextColor(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorTextMinimum.png)

Set color Text minimum progress bar.


* **Parameters**

    **color** (*QColor*) – color object of minimum progress bar text.



#### showExtremumsTexts()


![image](pictures/QRangeProgressBar_HideRangeTexts.png)

Show text of maximum/minimum progress bar.


#### class rangewidgets.TextsRange()


![image](docs/source/classes/TextsRange.png)

Texts range features object.


#### getEndLabelPosition()
Get end label position.


* **Returns**

    End label position.



* **Return type**

    string



#### getEndText()

* **Returns**

    Get fixed text of end range progress.



* **Return type**

    string



#### getEndTextColor()


![image](pictures/QRangeProgressBar_ColorTextEnd.png)

Get color Text end range.


#### getEndTextSpacer()

* **Returns**

    Space between end label and end range bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getRangeMinimumTextsSpacer()

* **Returns**

    Space between labels into range bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getRangeTextsFont()

* **Returns**

    Range text font.



* **Return type**

    Qfont



#### getRangeTextsFontSize()

* **Returns**

    Text range font size.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getRangeTextsScale()

* **Returns**

    Scale range text (between 0.0 to 1.0).



* **Return type**

    [float](https://docs.python.org/3/library/functions.html#float)



#### getStartLabelPosition()
Get start label position.


* **Returns**

    Start label position.



* **Return type**

    string



#### getStartText()

* **Returns**

    Get fixed text of start range progress.



* **Return type**

    string



#### getStartTextColor()


![image](pictures/QRangeProgressBar_ColorTextStart.png)

Get color Text start range.


#### getStartTextSpacer()

* **Returns**

    Space between start label and start range bar.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### hideRangeTexts()


![image](pictures/QRangeProgressBar_HideRangeExtremums.png)

Hide text of range.



![image](pictures/QRangeProgressBar_HideRangeTexts.png)

or with showExtremumsTexts():


#### isRangeTextsInside()

* **Returns**

    Range text is position inside.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isRangeTextsOutside()

* **Returns**

    Range text is position outside.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isRangeTextsShow()

* **Returns**

    State of range texts show.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### setEndLabelPositionBottom()
Set end label to bottom position.


#### setEndLabelPositionMiddle()
Set end label to middle position.


#### setEndLabelPositionMiddleBottom()
Set end label to middle bottom position.


#### setEndLabelPositionMiddleTop()
Set end label to middle top position.


#### setEndLabelPositionTop()
Set end label to top position.


#### setEndText(text=None)
Set fixed text for end range progress.


* **Parameters**

    **text** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – Text of end range progress



#### setEndTextColor(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorTextEnd.png)

Set color Text end range.


* **Parameters**

    **color** (*QColor*) – color object of end range text.



#### setEndTextSpacer(value=1)
Set spacer between end label and end range bar.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Value of spacer.



#### setRangeMinimumTextsSpacer(value=1)
Set spacer between labels into range bar.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Value of spacer.



#### setRangeTextInside()
Set range text inside position.


#### setRangeTextsFont(font=<PyQt5.QtGui.QFont object>)
Set text range font.


* **Parameters**

    **font** (*QFont*) – font object of start and end range text



#### setRangeTextsFontSize(size=10)
Set size range text font.


* **Parameters**

    **size** ([*int*](https://docs.python.org/3/library/functions.html#int)) – size of start and end range text font.



#### setRangeTextsOutside()
Set range text inside position.


#### setRangeTextsScale(scale=0.4)
Set scale of range text.


* **Parameters**

    **scale** ([*float*](https://docs.python.org/3/library/functions.html#float)) – scale of range text. 1.0 ≥ scale ≥ 0.01.



#### setStartLabelPositionBottom()
Set start label to bottom position.


#### setStartLabelPositionMiddle()
Set start label to middle position.


#### setStartLabelPositionMiddleBottom()
Set start label to middle bottom position.


#### setStartLabelPositionMiddleTop()
Set start label to middle top position.


#### setStartLabelPositionTop()
Set start label to top position.


#### setStartText(text=None)
Set fixed text for start range progress.


* **Parameters**

    **text** ([*str*](https://docs.python.org/3/library/stdtypes.html#str)) – Text of start range progress



#### setStartTextColor(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorTextStart.png)

Set color Text start range.


* **Parameters**

    **color** (*QColor*) – color object of start range text.



#### setStartTextSpacer(value=1)
Set spacer between start label and start range bar.


* **Parameters**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Value of spacer.



#### showRangeTexts()


![image](pictures/QRangeProgressBar_HideExtremumsTexts.png)

Show text of range.


#### class rangewidgets.DrawTexts()


![image](docs/source/classes/DrawTexts.png)

Draw texts object.


#### calculateMaximumLabelPosition(qp, height)
Caculate the position of maximum label.


* **Parameters**

    * **qp** (*QPainter*) – object of bar draw.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



* **Returns**

    property labelmaxxposition: horizontal position of maximum label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    property labelmaxyposition: vertical position of maximum label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    property textmax: Text of maximum label.



* **Return type**

    string



#### calculateMinimumLabelPosition(qp, height)
Caculate the position of minimum label.


* **Parameters**

    * **qp** (*QPainter*) – object of bar draw.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



* **Returns**

    property labelminxposition: horizontal position of minimum label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    property labelminyposition: vertical position of minimum label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    property textmin: Text of minimum label.



* **Return type**

    string



#### calculateRangeLabelsPositions(qp, height)
Calculate horizontal values of range labels.


* **Parameters**

    * **qp** (*QPainter*) – QPainter object drawing.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Height of QPainter drawing.



* **Returns**

    horizontalpositionstartlabel – Horizontal position of start label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    verticalpositionstartlabel – vertical position of start label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    pointsizestarttext – Size of start text.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    horizontalpositionendlabel – Horizontal position of end label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    verticalpositionendlabel – Vertical position of end label.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    pointsizeendtext – Size of end text.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### hideTexts()


![image](pictures/QRangeProgressBar_LevelValues.png)

Hide text of progress bar.


#### isTextsShow()

* **Returns**

    State text show from progress bar.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### scaleText(qp, text, height)
Scale a text for a box draw


* **Parameters**

    * **qp** (*QPainter*) – QPainter object drawing.

    * **text** (*string*) – text for scale calculate

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of box draw text.



* **Returns**

    textwidth – width of text.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



* **Returns**

    textheight – heigth of text.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### showTexts()


![image](pictures/QRangeProgressBar.png)

Show text of progress bar.


#### updateDrawTexts(qp, height)
Draw texts of bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### class rangewidgets.DrawLevels()


![image](docs/source/classes/DrawLevels.png)

Levels features object.


#### getColorAlert()


![image](pictures/QRangeProgressBar_ColorAlert.png)


* **Returns**

    Get color of alert level.



* **Return type**

    QColor



#### getColorSquareAlert()


![image](pictures/QRangeProgressBar_ColorSquareAlert.png)


* **Returns**

    Get color square of alert level.



* **Return type**

    QColor



#### getColorSquareWarning()


![image](pictures/QRangeProgressBar_ColorSquareWarning.png)


* **Returns**

    Get color square of warning level.



* **Return type**

    QColor



#### getColorWarning()


![image](pictures/QRangeProgressBar_ColorWarning.png)


* **Returns**

    Get color of warning level.



* **Return type**

    QColor



#### setColorAlert(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorAlert.png)

Set color of alert level.


* **Parameters**

    **color** (*QColor*) – color object of alert.



#### setColorSquareAlert(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorSquareAlert.png)

Set color square of alert.


* **Parameters**

    **color** (*QColor*) – color object of square alert.



#### setColorSquareWarning(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorSquareWarning.png)

Set color square of warning level.


* **Parameters**

    **color** (*QColor*) – color object of square warning.



#### setColorWarning(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorWarning.png)

Set color of warning level.


* **Parameters**

    **color** (*QColor*) – color object of warning.



#### updateDrawLevels(qp, height)
Draw Levels of bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### class rangewidgets.DrawGraduations()


![image](docs/source/classes/DrawGraduations.png)

Graduations features object.


#### getColorTextGraduations()


![image](pictures/QRangeProgressBar_ColorTextGraduations.png)


* **Returns**

    Get color Text graduations



* **Return type**

    QColor



#### getGraduationFont()

* **Returns**

    Get graduation font.



* **Return type**

    QFont



#### getGraduationFontSize()

* **Returns**

    Get graduation font size.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getGraduationsPosition()


![image](pictures/QRangeProgressBar_GraduationsPositions.png)


* **Returns**

    Get graduations position.



* **Return type**

    string



#### getLineGraduationLength()

* **Returns**

    Get line graduation length.



* **Return type**

    [int](https://docs.python.org/3/library/functions.html#int)



#### getListLabelsGraduations()

* **Returns**

    Get list labels graduations.



* **Return type**

    [list](https://docs.python.org/3/library/stdtypes.html#list)



#### getPositionTextCenterGraduations()


![image](pictures/QRangeProgressBar_GraduationsCenterTextPosition.png)


* **Returns**

    Get text position for center graduations.



* **Return type**

    string



#### hideGraduations()


![image](pictures/QRangeProgressBar.png)

Hide graduation of progress bar.


#### hideTextsGraduations()


![image](pictures/QRangeProgressBar_Graduations_Center.png)

Hide text graduation of progress bar.


#### isGraduationsShow()

* **Returns**

    State show graduation of progress bar.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### isGraduationsShowText()

* **Returns**

    Show text graduation state.



* **Return type**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### setColorTextGraduations(color=<PyQt5.QtGui.QColor object>)


![image](pictures/QRangeProgressBar_ColorTextGraduations.png)

Set color Text graduations.


* **Parameters**

    **color** (*QColor*) – color object of graduations texts.



#### setGraduationFont(font=<PyQt5.QtGui.QFont object>)
Set graduation font.


* **Parameters**

    **font** (*QFont*) – font object of graduations texts.



#### setGraduationFontSize(size=7)
Set graduation font size.


* **Parameters**

    **size** ([*int*](https://docs.python.org/3/library/functions.html#int)) – size of graduations texts.



#### setGraduationsPositionBottom()


![image](pictures/QRangeProgressBar_Graduations_Bottom.png)

Set graduations bottom.


#### setGraduationsPositionCenter()


![image](pictures/QRangeProgressBar_Graduations_Center.png)

Set graduations center.


#### setGraduationsPositionExpand()


![image](pictures/QRangeProgressBar_Graduations_Expand.png)

Set graduations expand.


#### setGraduationsPositionTop()


![image](pictures/QRangeProgressBar_Graduations_Top.png)

Set graduations top.


#### setGraduationsPositionTopBottom()


![image](pictures/QRangeProgressBar_Graduations_TopBottom.png)

Set graduations top and bottom.


#### setLineGraduationLength(sizeline=5)
Set line graduation length.


* **Parameters**

    **size** ([*int*](https://docs.python.org/3/library/functions.html#int)) – size of graduations lengths.



#### setListLabelGraduations(listlabels=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90])
Set list labels graduations.


* **Parameters**

    **listlabels** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)) – list of real values labels.



#### setPositionTextBottomCenterGraduations()


![image](pictures/QRangeProgressBar_Graduations_Center_BottomText.png)

Set text graduations center to bottom.


#### setPositionTextTopCenterGraduations()


![image](pictures/QRangeProgressBar_Graduations_Center_TopText.png)

Set text graduations center to top.


#### showGraduations()


![image](pictures/QRangeProgressBar_Graduations_Center_TopText.png)

Show graduation of progress bar.


#### showTextsGraduations()


![image](pictures/QRangeProgressBar_TextGraduations.png)

Show text graduation of progress bar.


#### updateDrawGraduations(qp, height)
Draw graduations of bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### class rangewidgets.QRangeProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)


![image](docs/source/classes/QRangeProgressBar.png)

Create a range progress bar widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeProgressBar.png)


#### initProgress()
Initialize QWidget parameters default UI.


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### updateDrawChunk(qp, height)
Draw chunk bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### updateDrawRange(qp, height)
Draw chunk bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### updateDrawSquareChunk(qp, height)
Draw square chunk bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### class rangewidgets.QRangeTextsProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)


![image](docs/source/classes/QRangeTextsProgressBar.png)

Create a range with labels progress bar widget.


* **Parameters**

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeTextsProgressBar.png)


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### class rangewidgets.QRangeGraduatedProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)


![image](docs/source/classes/QRangeGraduatedProgressBar.png)

Create a range with graduations progress bar widget.


* **Parameters**

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeGraduatedProgressBar.png)


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### class rangewidgets.QRangeGraduatedTextsProgressBar(minimum, maximum, start, end, islogscale, logarithmbase)


![image](docs/source/classes/QRangeGraduatedTextsProgressBar.png)

Create a range with graduations progress bar widget.


* **Parameters**

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeGraduatedTextsProgressBar.png)


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### class rangewidgets.QRangeLevelsProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


![image](docs/source/classes/QRangeLevelsProgressBar.png)

Create a range with levels progress bar widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeLevelsProgressBar.png)


#### initProgress()
Initialize QWidget parameters default UI.


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### updateDrawChunk(qp, height)
Draw chunk bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### updateDrawRange(qp, height)
Draw chunk bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### updateDrawSquareChunk(qp, height)
Draw square chunk bar.


* **Parameters**

    * **qp** (*QPainter*) – object of bar.

    * **height** ([*int*](https://docs.python.org/3/library/functions.html#int)) – height of widget.



#### class rangewidgets.QRangeLevelsTextsProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


![image](docs/source/classes/QRangeLevelsTextsProgressBar.png)

Create a range with levels progress bar widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeLevelsTextsProgressBar.png)


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### class rangewidgets.QRangeLevelsGraduatedProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


![image](docs/source/classes/QRangeLevelsGraduatedProgressBar.png)

Create a range with levels progress bar widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeLevelsGraduatedProgressBar.png)


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### class rangewidgets.QRangeLevelsGraduatedTextsProgressBar(minimum, maximum, start, end, alert_min, warning_min, warning_max, alert_max, islogscale, logarithmbase)


![image](docs/source/classes/QRangeLevelsGraduatedTextsProgressBar.png)

Create a range with levels progress bar widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base.



#### drawWidget(qp)
Draw widget progress bar.



![image](pictures/QRangeLevelsGraduatedTextsProgressBar.png)


#### paintEvent(e)
Redraw widget progress bar on event repaint().


* **Parameters**

    **e** (*QEvent*) – event of widget.



#### class rangewidgets.QFactoryRangeProgressBar()


![image](pictures/QFactoryRangeProgressBar.png)

Create a range progress bar widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of plage values.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of plages value.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale – default False.

    * **isaddtexts** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add indicators text for progress bar – default False.

    * **isgraduated** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add graduations for progress bar – default False.

    * **islevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add levels for progress bar – default False.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value – default None.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value – default None.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value – default None.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value – default None.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base – default 10.


## Slide Range Class


#### class rangewidgets.QSlideRange(minimum, maximum, start, end, islogscale=False, isaddtexts=False, isgraduated=False, islevels=False, alert_min=None, warning_min=None, warning_max=None, alert_max=None, logarithmbase=10)


![image](docs/source/classes/QSlideRange.png)

Create a slide range widget.


* **Parameters**

    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of progress bar.

    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of progress bar.

    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale – default False.

    * **isaddtexts** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add indicators text for progress bar.

    * **isgraduated** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Show the isgraduated progress bar.

    * **islevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add levels for progress bar – default False.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value – default None.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value – default None.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value – default None.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value – default None.

    * **logarithmbase** (

      ![image](pictures/QSlideRange.png)

      ) – Choose logarithm value base – default 10.



#### getListEndStepSlide()
Get list of end steps slide


* **Returns**

    End steps slide.



* **Return type**

    [tuple](https://docs.python.org/3/library/stdtypes.html#tuple)



#### getListStartStepSlide()
Get list of start steps slide


* **Returns**

    Start steps slide.



* **Return type**

    [tuple](https://docs.python.org/3/library/stdtypes.html#tuple)



#### initUI(min_progress_bar, max_progress_bar, start_range, end_range, islogscale, addtexts, addgraduated, addlevels, alertmin, warningmin, warningmax, alertmax, logarithmbase)


![image](pictures/QSlideRange.png)

Initialize UI slide range widget


* **Parameters**

    * **min_progress_bar** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of progress bar.

    * **max_progress_bar** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of progress bar.

    * **start_range** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The start of range progress.

    * **end_range** ([*int*](https://docs.python.org/3/library/functions.html#int)) – The end of range progress.

    * **islogscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm state scale.

    * **addtexts** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add indicators text for progress bar.

    * **addgraduated** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Show the isgraduated progress bar.

    * **addlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Add levels for progress bar – default False.

    * **alert_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum alert value – default None.

    * **warning_min** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of minimum warning value – default None.

    * **warning_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum warning value – default None.

    * **alert_max** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Level of maximum alert value – default None.

    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Choose logarithm value base – default 10.



#### on_change_value_slider_end_range()
Set end range slide


#### on_change_value_slider_start_range()
Set start range slide


#### resizeEvent(event)
Update widget when resized


#### setListEndStepSlide(listend=None)
Set list of end steps slide


* **Parameters**

    **listend** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)) – List of step end range.



#### setListStartStepSlide(liststart)
Set list of start steps slide


* **Parameters**

    **liststart** ([*list*](https://docs.python.org/3/library/stdtypes.html#list)) – List of step start range.


# Indices and tables
