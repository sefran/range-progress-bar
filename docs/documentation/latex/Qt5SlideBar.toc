\babel@toc {french}{}\relax 
\contentsline {chapter}{\numberline {1}Slide Bar}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}The \sphinxstyleliteralintitle {\sphinxupquote {progressbar}} module}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}FactorySlide}{4}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Position cursor}{5}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Range slide}{5}{subsection.1.2.2}%
\contentsline {section}{\numberline {1.3}QBar}{7}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Position cursor}{8}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Range}{8}{subsection.1.3.2}%
\contentsline {chapter}{\numberline {2}Levels Bar}{11}{chapter.2}%
\contentsline {section}{\numberline {2.1}The \sphinxstyleliteralintitle {\sphinxupquote {levelsbar}} module}{11}{section.2.1}%
\contentsline {section}{\numberline {2.2}FactoryLevels}{12}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Minimum Alert Level}{13}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Minimum Warning Level}{14}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Maximum Warning Level}{14}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Maximum Alert Level}{15}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Alerts Levels}{16}{subsection.2.2.5}%
\contentsline {subsection}{\numberline {2.2.6}Warnings Levels}{16}{subsection.2.2.6}%
\contentsline {subsection}{\numberline {2.2.7}Minimum Levels}{17}{subsection.2.2.7}%
\contentsline {subsection}{\numberline {2.2.8}Maximums Levels}{17}{subsection.2.2.8}%
\contentsline {subsection}{\numberline {2.2.9}All Levels}{18}{subsection.2.2.9}%
\contentsline {section}{\numberline {2.3}QLevelsBar}{19}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Position cursor}{20}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Range slide}{20}{subsection.2.3.2}%
\contentsline {subsection}{\numberline {2.3.3}Minimum Alert Level}{21}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Minimum Warning Level}{21}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {2.3.5}Maximum Warning Level}{22}{subsection.2.3.5}%
\contentsline {subsection}{\numberline {2.3.6}Maximum Alert Level}{23}{subsection.2.3.6}%
\contentsline {subsection}{\numberline {2.3.7}Alerts Levels}{23}{subsection.2.3.7}%
\contentsline {subsection}{\numberline {2.3.8}Warnings Levels}{24}{subsection.2.3.8}%
\contentsline {subsection}{\numberline {2.3.9}Minimum Levels}{24}{subsection.2.3.9}%
\contentsline {subsection}{\numberline {2.3.10}Maximums Levels}{25}{subsection.2.3.10}%
\contentsline {subsection}{\numberline {2.3.11}All Levels}{26}{subsection.2.3.11}%
\contentsline {chapter}{\numberline {3}Scaled Bar}{27}{chapter.3}%
\contentsline {section}{\numberline {3.1}The \sphinxstyleliteralintitle {\sphinxupquote {scaledbar}} module}{27}{section.3.1}%
\contentsline {section}{\numberline {3.2}FactoryScales}{29}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Linear scale}{30}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Logarithm scale}{30}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Scale function}{31}{subsection.3.2.3}%
\contentsline {section}{\numberline {3.3}QScaleBar}{33}{section.3.3}%
\contentsline {chapter}{\numberline {4}Indices and tables}{35}{chapter.4}%
\contentsline {chapter}{Index des modules Python}{37}{section*.15}%
\contentsline {chapter}{Index}{39}{section*.16}%
