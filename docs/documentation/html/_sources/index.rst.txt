.. Qt5 Range widgets documentation master file, created by
   sphinx-quickstart on Thu Mar 21 13:01:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. only:: not html and not markdown

    .. image:: pictures/obsolescence.svg
        :alt: Code obsolescence Slide Range Widget
        :align: left
        :width: 250px


.. only:: html

    .. image:: https://sefran.frama.io/range-progress-bar/badges/obsolescence.svg
        :alt: Code obsolescence Progress Bar
        :align: left
        :width: 250px
        

.. only:: markdown

    .. image:: https://sefran.frama.io/range-progress-bar/badges/obsolescence.svg
        :alt: Code obsolescence Progress Bar
        :align: left
        :width: 250px
        

Qt5 slide bars
##############

.. only:: not html

    `QT5 Slide Bars HTML documentation <https://sefran.frama.io/range-progress-bar>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   
Welcome to Qt5 slide bars documentation!
   
Slide Bar
*********

.. only:: not html and not markdown

    .. image:: pictures/pylint_progressbar.svg
        :alt: Python code quality progressbar.py
        :class: with-shadow
        :align: right
        :width: 200px
        
        
.. only:: html

    .. image:: https://sefran.frama.io/range-progress-bar/badges/pylint_progressbar.svg
        :alt: Click to open report
        :target: :docslidebar:pylint/index_progressbar.html
        :class: with-shadow
        :align: left
        :width: 200px
        

.. only:: markdown

    .. image:: https://sefran.frama.io/range-progress-bar/badges/pylint_progressbar.svg
        :alt: Python code quality progressbar.py
        :class: with-shadow
        :align: left
        :width: 200px

    `progressbar.py Pylint report <https://sefran.frama.io/range-progress-bar/pylint/index_progressbar.html>`_
    

:mod: progressbar

.. automodule:: progressbar
   :undoc-members:


FactorySlide
============

.. autoclass:: progressbar.FactorySlide
    :members:

Position cursor
---------------

If **position** or **setcursor** are selected option.

.. py:class:: FactorySlide()
    :noindex:
 
    .. py:method:: get_position()
        :noindex:
        
        Get position cursor value.

        :returns: Value of position cursor.
        :rtype: int, float


    .. py:method:: set_position(value)
        :noindex:
        
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float

    
Range slide
-----------

If **start**, **end** or **setrange** are selected option.

.. py:class:: FactorySlide()
    :noindex:
 
    .. py:method:: get_start()
        :noindex:
        
        Get start range value.

        :returns: Value of start range.
        :rtype: int, float

        
    .. py:method:: set_start(value)
        :noindex:
        
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float

        
    .. py:method:: get_end()
        :noindex:
        
        Get end range value.

        :returns: Value of end range.
        :rtype: int, float

        
    .. py:method:: set_end(value)
        :noindex:
        
        Set end range value.

        :param value: End range value -- default value 80.
        :type value: int, float

    
.. only:: latex

    .. raw:: latex

        \clearpage

        
QBar
====
    
.. autoclass:: progressbar.QBar
    :members:

Position cursor
---------------

If **position** or **setcursor** are selected option.

.. py:class:: QBar()
    :noindex:
 
    .. py:method:: get_position()
        :noindex:
        
        Get position cursor value.

        :returns: Value of position cursor.
        :rtype: int, float


    .. py:method:: set_position(value)
        :noindex:
        
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float

    
Range
-----

If **start**, **end** or **setrange** are selected option.

.. py:class:: QBar()
    :noindex:
 
    .. py:method:: get_start()
        :noindex:
        
        Get start range value.

        :returns: Value of start range.
        :rtype: int, float

        
    .. py:method:: set_start(value)
        :noindex:
        
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float

        
    .. py:method:: get_end()
        :noindex:
        
        Get end range value.

        :returns: Value of end range.
        :rtype: int, float

        
    .. py:method:: set_start(value)
        :noindex:
        
        Set end range value.

        :param value: End range value -- default value 80.
        :type value: int, float

    
.. only:: latex

    .. raw:: latex

        \clearpage


Levels Bar
**********

.. only:: not html and not markdown

    .. image:: pictures/pylint_levelsbar.svg
        :alt: Python code quality levelsbar.py
        :class: with-shadow
        :align: right
        :width: 200px
        

.. only:: html

    .. image:: https://sefran.frama.io/range-progress-bar/badges/pylint_levelsbar.svg
        :alt: Click to open report
        :target: :docslidebar:pylint/index_levelsbar.html
        :class: with-shadow
        :align: left
        :width: 200px
        

.. only:: markdown

    .. image:: https://sefran.frama.io/range-progress-bar/badges/pylint_levelsbar.svg
        :alt: Python code quality levelsbar.py
        :class: with-shadow
        :align: left
        :width: 200px

    `levelsbar.py Pylint report <https://sefran.frama.io/range-progress-bar/pylint/index_levelsbar.html>`_
    

:mod: levelsbar

.. automodule:: levelsbar
   :undoc-members:

   
FactoryLevels
=============

.. autoclass:: levelsbar.FactoryLevels
   :members:

   
Minimum Alert Level
-------------------

If **alertmin**, **setalertlevelmin**, **setalertlevels**, **setminimumlevels** or **setalllevels** are selected option.
    
.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: get_minimum_alert()
        :noindex:
        
        Get end minimum alert level value.

        :returns: Minimum end alert level.
        :rtype: int, float

        
    .. py:method:: set_minimum_alert(value)
        :noindex:
        
        Set end minimum alert level value.

        :param value: Minimum end alert level.
        :type value: int, float

        
    .. py:method:: show_minimum_alert()
        :noindex:
        
        Show minimum alert.


    .. py:method:: hide_minimum_alert()
        :noindex:
        
        Hide minimum alert.


    .. py:method:: is_minimum_alert_show()
        :noindex:
        
        Set of minimum alert show.

        :returns: Set of Minimum alert show
        :rtype: bool


Minimum Warning Level
---------------------

If **warningmin**, **setwarninglevelmin**, **setwarninglevels**, **setminimumlevels** or **setalllevels** are selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: get_minimum_warning()
        :noindex:
        
        Get end warning minimum level value.

        :returns: Minimum end level warning.
        :rtype: int, float

        
    .. py:method:: set_minimum_warning(value)
        :noindex:
        
        Set end minimum warning level value.

        :param value: Minimum end warning level.
        :type value: int, float

        
    .. py:method:: show_minimum_warning()
        :noindex:
        
        Show minimum warning level.


    .. py:method:: hide_minimum_warning()
        :noindex:
        
        Hide minimum warning level.


    .. py:method:: is_minimum_warning_show()
        :noindex:
        
        Set of minimum warning show.

        :returns: Set of Minimum warning show
        :rtype: bool


Maximum Warning Level
---------------------

If **warningmax**, **setwarninglevelmax**, **setwarninglevels**, **setmaximumlevels** or **setalllevels** are selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: get_maximum_warning()
        :noindex:
        
        Get start maximum warning level value.

        :returns: Maximum start warning level.
        :rtype: int, float

        
    .. py:method:: set_maximum_warning(value)
        :noindex:
        
        Set start maximum warning level value.

        :param value: Maximum start warning level.
        :type value: int, float

        
    .. py:method:: show_maximum_warning()
        :noindex:
        
        Show maximum warning level.


    .. py:method:: hide_maximum_warning()
        :noindex:
        
        Hide maximum warning level.


    .. py:method:: is_maximum_warning_show()
        :noindex:
        
        Set of minimum warning show.

        :returns: Set of Maximum warning show
        :rtype: bool


Maximum Alert Level
-------------------

If **alertmax**, **setalertlevelmax**, **setalertlevels**, **setmaximumlevels** or **setalllevels** are selected option.
    
.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: get_minimum_alert()
        :noindex:
        
        Get end minimum alert level value.

        :returns: Minimum end alert level.
        :rtype: int, float

        
    .. py:method:: set_minimum_alert(value)
        :noindex:
        
        Set end minimum alert level value.

        :param value: Minimum end alert level.
        :type value: int, float

        
    .. py:method:: show_minimum_alert()
        :noindex:
        
        Show minimum alert.


    .. py:method:: hide_minimum_alert()
        :noindex:
        
        Hide minimum alert.


    .. py:method:: is_minimum_alert_show()
        :noindex:
        
        Set of minimum alert show.

        :returns: Set of Minimum alert show
        :rtype: bool

    
Alerts Levels
-------------

If **setalertlevels** is selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: show_alerts()
        :noindex:
        
        Show alerts levels.


    .. py:method:: hide_alerts()
        :noindex:
        
        Hide alerts levels.


    .. py:method:: is_alerts_show()
        :noindex:
        
        All alerts levels are show.

        :returns: Alerts show
        :rtype: bool

        
    .. py:method:: is_a_alert_show()
        :noindex:
        
        A alert level is show.

        :returns: A alert show
        :rtype: bool

     
Warnings Levels
---------------

If **setwarninglevels** is selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: show_warnings()
        :noindex:
        
        Show warnings levels.


    .. py:method:: hide_warnings()
        :noindex:
        
    Hide warnings levels.


    .. py:method:: is_warnings_show()
        :noindex:
        
        All warnings levels are show.

        :returns: Alerts show
        :rtype: bool

        
    .. py:method:: is_a_warning_show()
        :noindex:
        
        A warning level is show.

        :returns: A warning show
        :rtype: bool

     
Minimum Levels
--------------

If **setminimumlevels** is selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: show_minimums_levels()
        :noindex:
        
        Show minimums levels.


    .. py:method:: hide_minimums_levels()
        :noindex:
        
        Hide minimums levels.


    .. py:method:: is_minimum_levels_show()
        :noindex:
        
        All minimum levels are show.

        :returns: Minimum levels show
        :rtype: bool

        
    .. py:method:: is_a_minimum_level_show()
        :noindex:
        
        A minimum level is show.

        :returns: A minimum show
        :rtype: bool

     
Maximums Levels
---------------

If **setmaximumlevels** is selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: show_maximums_levels()
        :noindex:
        
        Show maximums levels.


    .. py:method:: hide_maximums_levels()
        :noindex:
        
        Hide maximums levels.


    .. py:method:: is_maximums_levels_show()
        :noindex:
        
        All maximum levels are show.

        :returns: Maximum levels show
        :rtype: bool

        
    .. py:method:: is_a_maximum_level_show()
        :noindex:
        
        A maximum level is show.

        :returns: A maximum show
        :rtype: bool

     
All Levels
----------

If **setalllevels** is selected option.

.. py:class:: FactoryLevels()
    :noindex:
 
    .. py:method:: show_levels()
        :noindex:
        
        Show levels bar indicators.


    .. py:method:: hide_levels()
        :noindex:
        
        Hide levels bar indicators.


    .. py:method:: is_levels_show()
        :noindex:
        
        All levels are show.

        :returns: All levels show
        :rtype: bool

        
    .. py:method:: is_a_level_show()
        :noindex:
        
        A level is show.

        :returns: A level show
        :rtype: bool

     
.. only:: latex

    .. raw:: latex

        \clearpage

    
QLevelsBar
==========

.. autoclass:: levelsbar.QLevelsBar
    :members:

Position cursor
---------------

If **position** or **setcursor** are selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: get_position()
        :noindex:
        
        Get position cursor value.

        :returns: Value of position cursor.
        :rtype: int, float


    .. py:method:: set_position(value)
        :noindex:
        
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float
    
Range slide
-----------

If **start**, **end** or **setrange** are selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: get_start()
        :noindex:
        
        Get start range value.

        :returns: Value of start range.
        :rtype: int, float

        
    .. py:method:: set_start(value)
        :noindex:
        
        Set start range value.

        :param value: Start range value -- default value 20.
        :type value: int, float

        
    .. py:method:: get_end()
        :noindex:
        
        Get end range value.

        :returns: Value of end range.
        :rtype: int, float

        
    .. py:method:: set_start(value)
        :noindex:
        
        Set end range value.

        :param value: End range value -- default value 80.
        :type value: int, float

    
Minimum Alert Level
-------------------

If **alertmin**, **setalertlevelmin**, **setalertlevels**, **setminimumlevels** or **setalllevels** are selected option.
    
.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: get_minimum_alert()
        :noindex:
        
        Get end minimum alert level value.

        :returns: Minimum end alert level.
        :rtype: int, float

        
    .. py:method:: set_minimum_alert(value)
        :noindex:
        
        Set end minimum alert level value.

        :param value: Minimum end alert level.
        :type value: int, float

        
    .. py:method:: show_minimum_alert()
        :noindex:
        
        Show minimum alert.


    .. py:method:: hide_minimum_alert()
        :noindex:
        
        Hide minimum alert.


    .. py:method:: is_minimum_alert_show()
        :noindex:
        
        Set of minimum alert show.

        :returns: Set of Minimum alert show
        :rtype: bool


Minimum Warning Level
---------------------

If **warningmin**, **setwarninglevelmin**, **setwarninglevels**, **setminimumlevels** or **setalllevels** are selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: get_minimum_warning()
        :noindex:
        
        Get end warning minimum level value.

        :returns: Minimum end level warning.
        :rtype: int, float

        
    .. py:method:: set_minimum_warning(value)
        :noindex:
        
        Set end minimum warning level value.

        :param value: Minimum end warning level.
        :type value: int, float

        
    .. py:method:: show_minimum_warning()
        :noindex:
        
        Show minimum warning level.


    .. py:method:: hide_minimum_warning()
        :noindex:
        
        Hide minimum warning level.


    .. py:method:: is_minimum_warning_show()
        :noindex:
        
        Set of minimum warning show.

        :returns: Set of Minimum warning show
        :rtype: bool


Maximum Warning Level
---------------------

If **warningmax**, **setwarninglevelmax**, **setwarninglevels**, **setmaximumlevels** or **setalllevels** are selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: get_maximum_warning()
        :noindex:
        
        Get start maximum warning level value.

        :returns: Maximum start warning level.
        :rtype: int, float

        
    .. py:method:: set_maximum_warning(value)
        :noindex:
        
        Set start maximum warning level value.

        :param value: Maximum start warning level.
        :type value: int, float

        
    .. py:method:: show_maximum_warning()
        :noindex:
        
        Show maximum warning level.


    .. py:method:: hide_maximum_warning()
        :noindex:
        
        Hide maximum warning level.


    .. py:method:: is_maximum_warning_show()
        :noindex:
        
        Set of minimum warning show.

        :returns: Set of Maximum warning show
        :rtype: bool


Maximum Alert Level
-------------------

If **alertmax**, **setalertlevelmax**, **setalertlevels**, **setmaximumlevels** or **setalllevels** are selected option.
    
.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: get_minimum_alert()
        :noindex:
        
        Get end minimum alert level value.

        :returns: Minimum end alert level.
        :rtype: int, float

        
    .. py:method:: set_minimum_alert(value)
        :noindex:
        
        Set end minimum alert level value.

        :param value: Minimum end alert level.
        :type value: int, float

        
    .. py:method:: show_minimum_alert()
        :noindex:
        
        Show minimum alert.


    .. py:method:: hide_minimum_alert()
        :noindex:
        
        Hide minimum alert.


    .. py:method:: is_minimum_alert_show()
        :noindex:
        
        Set of minimum alert show.

        :returns: Set of Minimum alert show
        :rtype: bool

    
Alerts Levels
-------------

If **setalertlevels** is selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: show_alerts()
        :noindex:
        
        Show alerts levels.


    .. py:method:: hide_alerts()
        :noindex:
        
        Hide alerts levels.


    .. py:method:: is_alerts_show()
        :noindex:
        
        All alerts levels are show.

        :returns: Alerts show
        :rtype: bool

        
    .. py:method:: is_a_alert_show()
        :noindex:
        
        A alert level is show.

        :returns: A alert show
        :rtype: bool

     
Warnings Levels
---------------

If **setwarninglevels** is selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: show_warnings()
        :noindex:
        
        Show warnings levels.


    .. py:method:: hide_warnings()
        :noindex:
        
    Hide warnings levels.


    .. py:method:: is_warnings_show()
        :noindex:
        
        All warnings levels are show.

        :returns: Alerts show
        :rtype: bool

        
    .. py:method:: is_a_warning_show()
        :noindex:
        
        A warning level is show.

        :returns: A warning show
        :rtype: bool

     
Minimum Levels
--------------

If **setminimumlevels** is selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: show_minimums_levels()
        :noindex:
        
        Show minimums levels.


    .. py:method:: hide_minimums_levels()
        :noindex:
        
        Hide minimums levels.


    .. py:method:: is_minimum_levels_show()
        :noindex:
        
        All minimum levels are show.

        :returns: Minimum levels show
        :rtype: bool

        
    .. py:method:: is_a_minimum_level_show()
        :noindex:
        
        A minimum level is show.

        :returns: A minimum show
        :rtype: bool

     
Maximums Levels
---------------

If **setmaximumlevels** is selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: show_maximums_levels()
        :noindex:
        
        Show maximums levels.


    .. py:method:: hide_maximums_levels()
        :noindex:
        
        Hide maximums levels.


    .. py:method:: is_maximums_levels_show()
        :noindex:
        
        All maximum levels are show.

        :returns: Maximum levels show
        :rtype: bool

        
    .. py:method:: is_a_maximum_level_show()
        :noindex:
        
        A maximum level is show.

        :returns: A maximum show
        :rtype: bool

     
All Levels
----------

If **setalllevels** is selected option.

.. py:class:: QLevelsBar()
    :noindex:
 
    .. py:method:: show_levels()
        :noindex:
        
        Show levels bar indicators.


    .. py:method:: hide_levels()
        :noindex:
        
        Hide levels bar indicators.


    .. py:method:: is_levels_show()
        :noindex:
        
        All levels are show.

        :returns: All levels show
        :rtype: bool

        
    .. py:method:: is_a_level_show()
        :noindex:
        
        A level is show.

        :returns: A level show
        :rtype: bool

     
.. only:: latex

   .. raw:: latex

       \clearpage


Scaled Bar
**********

.. only:: not html and not markdown

    .. image:: pictures/pylint_scaledbar.svg
        :alt: Python code quality scaledbar.py
        :class: with-shadow
        :align: left
        :width: 250px

.. only:: html

    .. image:: https://sefran.frama.io/range-progress-bar/badges/pylint_scaledbar.svg
        :alt: Click to open report
        :target: :docslidebar:pylint/index_scaledbar.html
        :class: with-shadow
        :align: left
        :width: 250px

.. only:: markdown

    .. image:: https://sefran.frama.io/range-progress-bar/badges/pylint_scaledbar.svg
        :alt: Python code quality scaledbar.py
        :class: with-shadow
        :align: left
        :width: 250px
        
    `scaledbar.py Pylint report <https://sefran.frama.io/range-progress-bar/pylint/index_scaledbar.html>`_

    
:mod: scaledbar

.. automodule:: scaledbar
   :undoc-members:

   
FactoryScales
=============

.. autoclass:: scaledbar.FactoryScales
   :members:


Linear scale
------------

if **setlinearscale**, **linearfactor** or **setallscales** are selected option.

.. py:class:: FactoryScales()
    :noindex:
 
    .. py:method:: get_linear_factor()
        :noindex:
        
        Get linear factor.

        :returns: Linear factor value.
        :rtype: int, float


    .. py:method:: set_linear_factor(value)
        :noindex:
        
        Set linear factor.

        :param value: Linear factor value.
        :type value: int, float        


Logarithm scale
---------------

if **setlogarithmscale**, **logarithmbase** or **setallscales** are selected option.

.. py:class:: FactoryScales()
    :noindex:
 
    .. py:method:: get_logarithm_base()
        :noindex:
        
        Get default logarithm base.

        :returns: Value of base logarithm.
        :rtype: int


    .. py:method:: set_logarithm_base(value)
        :noindex:
        
        Set logarithm base value.

        :param value: Base value of logrithm -- default value 10.
        :type value: int
        

    .. py:method:: get_log0()
        :noindex:
        
        Get default value log for value 0

        :returns: Value of 0 logarithm.
        :rtype: int


    .. py:method:: set_log0(value)
        :noindex:
        
        Set default value log for value 0
        
        .. Important::
            
            * value=None log(0) return Infinity.
            * value=NaN log(0) return NaN.
            * value=float log(0) return log(float)

        :param value: Value of log(0).
        :type value: None, NaN or float

        
Scale function
--------------

if **setlogarithmscale** is not selected and **setlinearscale** or **linearfactor** are selected option.

.. py:class:: FactoryScales()
    :noindex:
 
    .. py:method:: scale_value(value)
        :noindex:
        
        Return value scaled.
        
        If value is NaN, scale_value return None.

        :param value: value to scale.
        :type value: int, float
        :returns: Value scaled.
        :rtype: int, float


if **setlinearscale** is not selected and **setlogarithmscale** or **logarithmbase** are selected option.

.. py:class:: FactoryScales()
    :noindex:
 
    .. py:method:: scale_value(value)
        :noindex:
        
        Return signed log(abs(value))
        
        .. Important::
            
            * If value is NaN, scale_value return None.
            * If get_log0() is None, value=0 return Infinity.
            * If get_log0() is NaN, value=0 return NaN.
            * If get_log0() is a float, value=0 return log(float)

        :param value: value to scale.
        :type value: int, float
        :returns: Value scaled.
        :rtype: int, float


if **setlinearscale** (or **linearfactor**) and **setlogarithmscale** (or **logarithmbase**), or **setallscales** are selected option.

.. py:class:: FactoryScales()
    :noindex:
 
    .. py:method:: set_linear_scale()
        :noindex:
        
        Set linear scale step.


    .. py:method:: set_logarithm_scale()
        :noindex:
        
        Set logarithm scale step.


    .. py:method:: is_linear_scale()
        :noindex:
        
        Get linear step state.

        :returns: State of linear scale.
        :rtype: bool
        

    .. py:method:: is_logarithm_scale()
        :noindex:
        
        Get logarithm step state.

        :returns: State of logarithm scale.
        :rtype: bool
        

    .. py:method:: scale_value(value)
        :noindex:
        
        Return value scaled.

        If value is NaN, scale_value return None.
    
        .. Important::
        
            * For logarithm scale return signed log(abs(value))
            * If get_log0() is None, value=0 return Infinity.
            * If get_log0() is NaN, value=0 return NaN.
            * If get_log0() is a float, value=0 return log(float)

        :param value: value to scale.
        :type value: int, float
        :returns: Value scaled.
        :rtype: int, float


.. only:: latex

   .. raw:: latex
   
       \clearpage
   

QScaleBar
=========

.. .. autoclass:: rangewidgets.QScaleBar
..     :members:

.. .. only:: latex

..     .. raw:: latex

..         \clearpage


Indices and tables
******************
