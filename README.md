<!-- Qt5 Range widgets documentation master file, created by
sphinx-quickstart on Thu Mar 21 13:01:54 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->


![image](https://sefran.frama.io/range-progress-bar/badges/obsolescence.svg)

# Qt5 slide bars

[QT5 Slide Bars HTML documentation](https://sefran.frama.io/range-progress-bar)

Welcome to Qt5 slide bars documentation!

## Slide Bar



![image](https://sefran.frama.io/range-progress-bar/badges/pylint_progressbar.svg)

[progressbar.py Pylint report](https://sefran.frama.io/range-progress-bar/pylint/index_progressbar.html)


* **mod**

    progressbar



* **Date**

    2023-02-16



* **Revision**

    2.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)>



* **Description**

    This is a documention for python 3 bar slide api



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.



* **Release Life Cycle**

    Release candidate


### The `progressbar` module

Use it to import slide bar feature or class slide bar features.

> 

> ![image](docs/sources-documents/classes/classes_progressbar.png)

Example:

```python
from progressbar import QBar

start_range = 1
end_range = 45
min_range = 1
max_range = 100

myObject = QBar(min_range, max_range, start=start_range, end=end_range)
```

### FactorySlide


### _class_ progressbar.FactorySlide(name, bases, namespace, \*\*kwargs)


![image](docs/sources-documents/classes/FactorySlide.png)

Create a object with slide bar features.


* **Paramètres**

    
    * **position** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Position of cursor.


    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start of range.


    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – End of range.


    * **setcursor** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set cursor features.


    * **setrange** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set range features.


Example:

```default
from progressbar import FactorySlide

class Slide(FactorySlide, start=30, end=70)
    ''' Bar class '''
    def __init__():
        ''' Initialize Bar class '''
        self._maximum = 100
        self._minimum = 0
rangeslide = Slide()
print('Start of slide: %s' % rangeslide.get_start())
print('End of slide: %s' % rangeslide.get_end())
```

#### Position cursor

If **position** or **setcursor** are selected option.


### _class_ progressbar.FactorySlide()

#### get_position()
Get position cursor value.


* **Renvoie**

    Value of position cursor.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_position(value)
Set start range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.


#### Range slide

If **start**, **end** or **setrange** are selected option.


### _class_ progressbar.FactorySlide()

#### get_start()
Get start range value.


* **Renvoie**

    Value of start range.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_start(value)
Set start range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.



#### get_end()
Get end range value.


* **Renvoie**

    Value of end range.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_end(value)
Set end range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – End range value – default value 80.


### QBar


### _class_ progressbar.QBar(\*args, \*\*kwargs)


![image](docs/sources-documents/classes/QBar.png)

Create a position slide bar or range slide bar object.


* **Paramètres**

    
    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The minimum bar value.


    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The maximum bar value.


    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The start of range.


    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The end of range.


    * **position** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Position of cursor.


    * **start** – Start of range.


    * **end** – End of range.


    * **setcursor** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set cursor features.


    * **setrange** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set range features.


Example:

```default
from progressbar import QBar

barobject = QBar(1,100)
print('Minimum: %s' % rangeobject.get_minimum())
print('Maximum: %s' % rangeobject.get_maximum())

barobject = QBar(1,100,10,90)
print('Minimum: %s' % rangeobject.get_minimum())
print('Start: %s' % rangeobject.get_start())
print('End: %s' % rangeobject.get_end())
print('Maximum: %s' % rangeobject.get_maximum())
```


#### get_maximum()
Get maximum bar value.


* **Renvoie**

    Value of maximum bar.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### get_minimum()
Get minimum bar value.


* **Renvoie**

    Value of minimum bar.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_maximum(value)
Set maximum bar value


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Maximum bar value – default value 100.



#### set_minimum(value)
Set minimum bar value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum bar value – default value 0.


#### Position cursor

If **position** or **setcursor** are selected option.


### _class_ progressbar.QBar()

#### get_position()
Get position cursor value.


* **Renvoie**

    Value of position cursor.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_position(value)
Set start range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.


#### Range

If **start**, **end** or **setrange** are selected option.


### _class_ progressbar.QBar()

#### get_start()
Get start range value.


* **Renvoie**

    Value of start range.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_start(value)
Set start range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.



#### get_end()
Get end range value.


* **Renvoie**

    Value of end range.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_start(value)
Set end range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – End range value – default value 80.


## Levels Bar



![image](https://sefran.frama.io/range-progress-bar/badges/pylint_levelsbar.svg)

[levelsbar.py Pylint report](https://sefran.frama.io/range-progress-bar/pylint/index_levelsbar.html)


* **mod**

    levelsbar



* **Date**

    2023-02-16



* **Revision**

    2.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)>



* **Description**

    This is a documention for python 3 bar slide api



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.



* **Release Life Cycle**

    Release candidate


### The `levelsbar` module

Use it get class progress levels bar features.

> 

> ![image](docs/sources-documents/classes/classes_levelsbar.png)

Example:

```python
from levelsbar import QLevelsBar

min_bar = 1
max_bar = 100

myObject = QLevelsBar(min_bar, max_bar, start=1, end=45,
                alertmin=10, warningmin=30, warningmax=70, alertmax=90)
```

### FactoryLevels


### _class_ levelsbar.FactoryLevels(name, bases, namespace, \*\*kwargs)


![image](docs/sources-documents/classes/FactoryLevels.png)

Create a object with levels informations and functions.


* **Paramètres**

    
    * **alertmin** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of minimum alert value.


    * **warningmin** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of minimum warning value.


    * **warningmax** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of maximum warning value.


    * **alertmax** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of maximum alert value.


    * **setalertlevelmin** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set alert min features.


    * **setwarninglevelmin** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set warning min features.


    * **setwarninglevelmax** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set warning max features.


    * **setalertlevelmax** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set  alert max features.


    * **setalertlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set alert features.


    * **setwarninglevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set warning features.


    * **setminimumlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set minimum levels features.


    * **setmaximumlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set maximum levels features.


    * **setalllevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set all levels features.


Example:

```default
from rangewidgets import FactoryLevels

class TestClass(metaclass=FactoryLevels,
                alertmin=10, warningmin=20, warningmax=80, alertmax=90):
    pass

rangeobject = TestClass()
print('Minimum alert end: %s' % rangeobject.get_minimum_alert())
print('Minimum warning end: %s' % rangeobject.get_minimum_warning())
print('Maximum warning start: %s' % rangeobject.get_maximum_warning())
print('Maximum alert start: %s' % rangeobject.get_maximum_alert())
```

#### Minimum Alert Level

If **alertmin**, **setalertlevelmin**, **setalertlevels**, **setminimumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.FactoryLevels()

#### get_minimum_alert()
Get end minimum alert level value.


* **Renvoie**

    Minimum end alert level.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_minimum_alert(value)
Set end minimum alert level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end alert level.



#### show_minimum_alert()
Show minimum alert.


#### hide_minimum_alert()
Hide minimum alert.


#### is_minimum_alert_show()
Set of minimum alert show.


* **Renvoie**

    Set of Minimum alert show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Minimum Warning Level

If **warningmin**, **setwarninglevelmin**, **setwarninglevels**, **setminimumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.FactoryLevels()

#### get_minimum_warning()
Get end warning minimum level value.


* **Renvoie**

    Minimum end level warning.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_minimum_warning(value)
Set end minimum warning level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end warning level.



#### show_minimum_warning()
Show minimum warning level.


#### hide_minimum_warning()
Hide minimum warning level.


#### is_minimum_warning_show()
Set of minimum warning show.


* **Renvoie**

    Set of Minimum warning show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Maximum Warning Level

If **warningmax**, **setwarninglevelmax**, **setwarninglevels**, **setmaximumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.FactoryLevels()

#### get_maximum_warning()
Get start maximum warning level value.


* **Renvoie**

    Maximum start warning level.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_maximum_warning(value)
Set start maximum warning level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Maximum start warning level.



#### show_maximum_warning()
Show maximum warning level.


#### hide_maximum_warning()
Hide maximum warning level.


#### is_maximum_warning_show()
Set of minimum warning show.


* **Renvoie**

    Set of Maximum warning show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Maximum Alert Level

If **alertmax**, **setalertlevelmax**, **setalertlevels**, **setmaximumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.FactoryLevels()

#### get_minimum_alert()
Get end minimum alert level value.


* **Renvoie**

    Minimum end alert level.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_minimum_alert(value)
Set end minimum alert level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end alert level.



#### show_minimum_alert()
Show minimum alert.


#### hide_minimum_alert()
Hide minimum alert.


#### is_minimum_alert_show()
Set of minimum alert show.


* **Renvoie**

    Set of Minimum alert show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Alerts Levels

If **setalertlevels** is selected option.


### _class_ levelsbar.FactoryLevels()

#### show_alerts()
Show alerts levels.


#### hide_alerts()
Hide alerts levels.


#### is_alerts_show()
All alerts levels are show.


* **Renvoie**

    Alerts show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_alert_show()
A alert level is show.


* **Renvoie**

    A alert show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Warnings Levels

If **setwarninglevels** is selected option.


### _class_ levelsbar.FactoryLevels()

#### show_warnings()
Show warnings levels.


#### hide_warnings()
Hide warnings levels.


#### is_warnings_show()
All warnings levels are show.


* **Renvoie**

    Alerts show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_warning_show()
A warning level is show.


* **Renvoie**

    A warning show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Minimum Levels

If **setminimumlevels** is selected option.


### _class_ levelsbar.FactoryLevels()

#### show_minimums_levels()
Show minimums levels.


#### hide_minimums_levels()
Hide minimums levels.


#### is_minimum_levels_show()
All minimum levels are show.


* **Renvoie**

    Minimum levels show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_minimum_level_show()
A minimum level is show.


* **Renvoie**

    A minimum show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Maximums Levels

If **setmaximumlevels** is selected option.


### _class_ levelsbar.FactoryLevels()

#### show_maximums_levels()
Show maximums levels.


#### hide_maximums_levels()
Hide maximums levels.


#### is_maximums_levels_show()
All maximum levels are show.


* **Renvoie**

    Maximum levels show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_maximum_level_show()
A maximum level is show.


* **Renvoie**

    A maximum show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### All Levels

If **setalllevels** is selected option.


### _class_ levelsbar.FactoryLevels()

#### show_levels()
Show levels bar indicators.


#### hide_levels()
Hide levels bar indicators.


#### is_levels_show()
All levels are show.


* **Renvoie**

    All levels show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_level_show()
A level is show.


* **Renvoie**

    A level show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


### QLevelsBar


### _class_ levelsbar.QLevelsBar(\*args, \*\*kwargs)


![image](docs/sources-documents/classes/QLevelsBar.png)

Create a levels bar object.


* **Paramètres**

    
    * **minimum** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The minimum bar value.


    * **maximum** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The maximum bar value.


    * **start** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The start of range.


    * **end** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – The end of range.


    * **position** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Position of cursor.


    * **start** – Start of range.


    * **end** – End of range.


    * **setcursor** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set cursor features.


    * **setrange** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set range features.


    * **alertmin** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of minimum alert value.


    * **warningmin** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of minimum warning value.


    * **warningmax** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of maximum warning value.


    * **alertmax** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Level of maximum alert value.


    * **setalertlevelmin** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set alert min features.


    * **setwarninglevelmin** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set warning min features.


    * **setwarninglevelmax** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set warning max features.


    * **setalertlevelmax** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set  alert max features.


    * **setalertlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set alert features.


    * **setwarninglevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set warning features.


    * **setminimumlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set minimum levels features.


    * **setmaximumlevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set maximum levels features.


    * **setalllevels** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Set all levels features.


Example:

```default
from rangewidgets import QLevelsBar

rangeobject = QLevelsBar(1,100,start=20,end=80)
print('Minimum: %s' % rangeobject.get_minimum())
print('Start: %s' % rangeobject.get_start())
print('End: %s' % rangeobject.get_end())
print('Maximum: %s' % rangeobject.get_maximum())
```

#### Position cursor

If **position** or **setcursor** are selected option.


### _class_ levelsbar.QLevelsBar()

#### get_position()
Get position cursor value.


* **Renvoie**

    Value of position cursor.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_position(value)
Set start range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.


#### Range slide

If **start**, **end** or **setrange** are selected option.


### _class_ levelsbar.QLevelsBar()

#### get_start()
Get start range value.


* **Renvoie**

    Value of start range.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_start(value)
Set start range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Start range value – default value 20.



#### get_end()
Get end range value.


* **Renvoie**

    Value of end range.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_start(value)
Set end range value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – End range value – default value 80.


#### Minimum Alert Level

If **alertmin**, **setalertlevelmin**, **setalertlevels**, **setminimumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.QLevelsBar()

#### get_minimum_alert()
Get end minimum alert level value.


* **Renvoie**

    Minimum end alert level.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_minimum_alert(value)
Set end minimum alert level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end alert level.



#### show_minimum_alert()
Show minimum alert.


#### hide_minimum_alert()
Hide minimum alert.


#### is_minimum_alert_show()
Set of minimum alert show.


* **Renvoie**

    Set of Minimum alert show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Minimum Warning Level

If **warningmin**, **setwarninglevelmin**, **setwarninglevels**, **setminimumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.QLevelsBar()

#### get_minimum_warning()
Get end warning minimum level value.


* **Renvoie**

    Minimum end level warning.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_minimum_warning(value)
Set end minimum warning level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end warning level.



#### show_minimum_warning()
Show minimum warning level.


#### hide_minimum_warning()
Hide minimum warning level.


#### is_minimum_warning_show()
Set of minimum warning show.


* **Renvoie**

    Set of Minimum warning show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Maximum Warning Level

If **warningmax**, **setwarninglevelmax**, **setwarninglevels**, **setmaximumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.QLevelsBar()

#### get_maximum_warning()
Get start maximum warning level value.


* **Renvoie**

    Maximum start warning level.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_maximum_warning(value)
Set start maximum warning level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Maximum start warning level.



#### show_maximum_warning()
Show maximum warning level.


#### hide_maximum_warning()
Hide maximum warning level.


#### is_maximum_warning_show()
Set of minimum warning show.


* **Renvoie**

    Set of Maximum warning show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Maximum Alert Level

If **alertmax**, **setalertlevelmax**, **setalertlevels**, **setmaximumlevels** or **setalllevels** are selected option.


### _class_ levelsbar.QLevelsBar()

#### get_minimum_alert()
Get end minimum alert level value.


* **Renvoie**

    Minimum end alert level.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_minimum_alert(value)
Set end minimum alert level value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Minimum end alert level.



#### show_minimum_alert()
Show minimum alert.


#### hide_minimum_alert()
Hide minimum alert.


#### is_minimum_alert_show()
Set of minimum alert show.


* **Renvoie**

    Set of Minimum alert show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Alerts Levels

If **setalertlevels** is selected option.


### _class_ levelsbar.QLevelsBar()

#### show_alerts()
Show alerts levels.


#### hide_alerts()
Hide alerts levels.


#### is_alerts_show()
All alerts levels are show.


* **Renvoie**

    Alerts show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_alert_show()
A alert level is show.


* **Renvoie**

    A alert show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Warnings Levels

If **setwarninglevels** is selected option.


### _class_ levelsbar.QLevelsBar()

#### show_warnings()
Show warnings levels.


#### hide_warnings()
Hide warnings levels.


#### is_warnings_show()
All warnings levels are show.


* **Renvoie**

    Alerts show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_warning_show()
A warning level is show.


* **Renvoie**

    A warning show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Minimum Levels

If **setminimumlevels** is selected option.


### _class_ levelsbar.QLevelsBar()

#### show_minimums_levels()
Show minimums levels.


#### hide_minimums_levels()
Hide minimums levels.


#### is_minimum_levels_show()
All minimum levels are show.


* **Renvoie**

    Minimum levels show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_minimum_level_show()
A minimum level is show.


* **Renvoie**

    A minimum show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### Maximums Levels

If **setmaximumlevels** is selected option.


### _class_ levelsbar.QLevelsBar()

#### show_maximums_levels()
Show maximums levels.


#### hide_maximums_levels()
Hide maximums levels.


#### is_maximums_levels_show()
All maximum levels are show.


* **Renvoie**

    Maximum levels show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_maximum_level_show()
A maximum level is show.


* **Renvoie**

    A maximum show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


#### All Levels

If **setalllevels** is selected option.


### _class_ levelsbar.QLevelsBar()

#### show_levels()
Show levels bar indicators.


#### hide_levels()
Hide levels bar indicators.


#### is_levels_show()
All levels are show.


* **Renvoie**

    All levels show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_a_level_show()
A level is show.


* **Renvoie**

    A level show



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)


## Scaled Bar



![image](https://sefran.frama.io/range-progress-bar/badges/pylint_scaledbar.svg)

[scaledbar.py Pylint report](https://sefran.frama.io/range-progress-bar/pylint/index_scaledbar.html)


* **mod**

    scaledbar



* **Date**

    2023-02-16



* **Revision**

    2.0



* **Author**

    Franc SERRES <[sefran007@gmail.com](mailto:sefran007@gmail.com)>



* **Description**

    This is a documention for python 3 bar slide api



* **Info**

    See <[https://framagit.org/sefran/range-progress-bar](https://framagit.org/sefran/range-progress-bar)> for more recent info.



* **Release Life Cycle**

    Alpha


### The `scaledbar` module

Use it get class progress levels bar features.

> 

> ![image](docs/sources-documents/classes/classes_scaledbar.png)

Example:

```python
from scaledbar import QScaledBar

start_range = 1
end_range = 45
min_range = 1
max_range = 100
alertmin = 10
warningmin = 30
warningmax = 70
alertmax = 90

myObject = QScaledBar(min_range, max_range, start_range, end_range,
                  alertmin, warningmin, warningmax, alertmax,
                  islogscale=True)
```

### FactoryScales


### _class_ scaledbar.FactoryScales(name, bases, namespace, \*\*kwargs)


![image](docs/sources-documents/classes/FactoryScales.png)

Create object with linear or logarithm features scales.


* **Paramètres**

    
    * **linearfactor** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Set scale factor.


    * **logarithmbase** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Set logarithm value base.


    * **setlinearscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose linear scale system.


    * **setlogarithmscale** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose logarithm scale system.


    * **setallscales** ([*bool*](https://docs.python.org/3/library/functions.html#bool)) – Choose all scales systems (linear and logarithm).


Example:

```default
from scaledbar import FactoryScales

class Scale(FactoryScales, setlogarithmscale=True)
    ''' Scale class '''
    def __init__():
        ''' Initialize Scale class '''
        pass
object = Scale()
```

#### Linear scale

if **setlinearscale**, **linearfactor** or **setallscales** are selected option.


### _class_ scaledbar.FactoryScales()

#### get_linear_factor()
Get linear factor.


* **Renvoie**

    Linear factor value.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)



#### set_linear_factor(value)
Set linear factor.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – Linear factor value.


#### Logarithm scale

if **setlogarithmscale**, **logarithmbase** or **setallscales** are selected option.


### _class_ scaledbar.FactoryScales()

#### get_logarithm_base()
Get default logarithm base.


* **Renvoie**

    Value of base logarithm.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int)



#### set_logarithm_base(value)
Set logarithm base value.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)) – Base value of logrithm – default value 10.



#### get_log0()
Get default value log for value 0


* **Renvoie**

    Value of 0 logarithm.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int)



#### set_log0(value)
Set default value log for value 0


* **Paramètres**

    **value** (*None**, **NaN** or *[*float*](https://docs.python.org/3/library/functions.html#float)) – Value of log(0).


#### Scale function

if **setlogarithmscale** is not selected and **setlinearscale** or **linearfactor** are selected option.


### _class_ scaledbar.FactoryScales()

#### scale_value(value)
Return value scaled.

If value is NaN, scale_value return None.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – value to scale.



* **Renvoie**

    Value scaled.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)


if **setlinearscale** is not selected and **setlogarithmscale** or **logarithmbase** are selected option.


### _class_ scaledbar.FactoryScales()

#### scale_value(value)
Return signed log(abs(value))


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – value to scale.



* **Renvoie**

    Value scaled.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)


if **setlinearscale** (or **linearfactor**) and **setlogarithmscale** (or **logarithmbase**), or **setallscales** are selected option.


### _class_ scaledbar.FactoryScales()

#### set_linear_scale()
Set linear scale step.


#### set_logarithm_scale()
Set logarithm scale step.


#### is_linear_scale()
Get linear step state.


* **Renvoie**

    State of linear scale.



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### is_logarithm_scale()
Get logarithm step state.


* **Renvoie**

    State of logarithm scale.



* **Type renvoyé**

    [bool](https://docs.python.org/3/library/functions.html#bool)



#### scale_value(value)
Return value scaled.

If value is NaN, scale_value return None.


* **Paramètres**

    **value** ([*int*](https://docs.python.org/3/library/functions.html#int)*, *[*float*](https://docs.python.org/3/library/functions.html#float)) – value to scale.



* **Renvoie**

    Value scaled.



* **Type renvoyé**

    [int](https://docs.python.org/3/library/functions.html#int), [float](https://docs.python.org/3/library/functions.html#float)


### QScaleBar

<!-- .. autoclass:: rangewidgets.QScaleBar -->
<!-- :members: -->
<!-- .. only:: latex -->
<!-- .. raw:: latex -->
<!-- \clearpage -->
## Indices and tables
