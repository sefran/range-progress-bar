import unittest
from progressbar import FactorySlide

class TestFactorySlide(unittest.TestCase):
    """Test FactorySlide features"""

    def test_methods_default_initialization(self):
        """Test default initialisation methods"""
        class A(metaclass=FactorySlide):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = A()
        self.assertFalse(hasattr(slidetest, '_position'), "Present attribute '_position'")
        self.assertFalse(hasattr(slidetest, 'get_position'), "Present attribute 'get_position'")
        self.assertFalse(hasattr(slidetest, 'set_position'), "Present attribute 'set_position'")
        self.assertFalse(hasattr(slidetest, '_end'), "Present attribute '_end'")
        self.assertFalse(hasattr(slidetest, '_start'), "Present attribute '_start'")
        self.assertFalse(hasattr(slidetest, 'nulmaxrange'), "Present attribute 'nulmaxrange'")
        self.assertFalse(hasattr(slidetest, 'nulminrange'), "Present attribute 'nulminrange'")
        self.assertFalse(hasattr(slidetest, 'get_end'), "Present attribute 'get_end'")
        self.assertFalse(hasattr(slidetest, 'get_start'), "Present attribute 'get_start'")
        self.assertFalse(hasattr(slidetest, 'set_end'), "Present attribute 'set_end'")
        self.assertFalse(hasattr(slidetest, 'set_start'), "Present attribute 'set_start'")

    def test_methods_initialization_with_setcursor(self):
        """Test initialisation methods with alertlevel"""
        class B(metaclass=FactorySlide, setcursor=True):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = B()
        self.assertTrue(hasattr(slidetest, '_position'), "Missing attribute '_position'")
        self.assertTrue(hasattr(slidetest, 'get_position'), "Missing attribute 'get_position'")
        self.assertTrue(hasattr(slidetest, 'set_position'), "Missing attribute 'set_position'")
        self.assertFalse(hasattr(slidetest, '_end'), "Present attribute '_end'")
        self.assertFalse(hasattr(slidetest, '_start'), "Present attribute '_start'")
        self.assertFalse(hasattr(slidetest, 'nulmaxrange'), "Present attribute 'nulmaxrange'")
        self.assertFalse(hasattr(slidetest, 'nulminrange'), "Present attribute 'nulminrange'")
        self.assertFalse(hasattr(slidetest, 'get_end'), "Present attribute 'get_end'")
        self.assertFalse(hasattr(slidetest, 'get_start'), "Present attribute 'get_start'")
        self.assertFalse(hasattr(slidetest, 'set_end'), "Present attribute 'set_end'")
        self.assertFalse(hasattr(slidetest, 'set_start'), "Present attribute 'set_start'")
        self.assertIsNone(slidetest.get_position(), "Position value should be None")
        slidetest.set_position(30)
        self.assertEqual(slidetest.get_position(), 30, "Position should be 30")

    def test_methods_initialization_with_setrange(self):
        """Test initialisation methods with alertlevel"""
        class C(metaclass=FactorySlide, setrange=True):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = C()
        self.assertFalse(hasattr(slidetest, '_position'), "Present attribute '_position'")
        self.assertFalse(hasattr(slidetest, 'get_position'), "Present attribute 'get_position'")
        self.assertFalse(hasattr(slidetest, 'set_position'), "Present attribute 'set_position'")
        self.assertTrue(hasattr(slidetest, '_end'), "Missing attribute '_end'")
        self.assertTrue(hasattr(slidetest, '_start'), "Missing attribute '_start'")
        self.assertTrue(hasattr(slidetest, 'nulmaxrange'), "Missing attribute 'nulmaxrange'")
        self.assertTrue(hasattr(slidetest, 'nulminrange'), "Missing attribute 'nulminrange'")
        self.assertTrue(hasattr(slidetest, 'get_end'), "Missing attribute 'get_end'")
        self.assertTrue(hasattr(slidetest, 'get_start'), "Missing attribute 'get_start'")
        self.assertTrue(hasattr(slidetest, 'set_end'), "Missing attribute 'set_end'")
        self.assertTrue(hasattr(slidetest, 'set_start'), "Missing attribute 'set_start'")
        self.assertIsNone(slidetest.get_end(), "End value should be None")
        self.assertIsNone(slidetest.get_start(), "Start value should be None")
        slidetest.set_end(90)
        slidetest.set_start(10)
        self.assertEqual(slidetest.get_end(), 90, "End should be 90")
        self.assertEqual(slidetest.get_start(), 10, "Start should be 10")

    def test_methods_initialization_with_position(self):
        """Test initialisation methods with alertlevel"""
        class D(metaclass=FactorySlide, position=30):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = D()
        self.assertTrue(hasattr(slidetest, '_position'), "Present attribute '_position'")
        self.assertTrue(hasattr(slidetest, 'get_position'), "Present attribute 'get_position'")
        self.assertTrue(hasattr(slidetest, 'set_position'), "Present attribute 'set_position'")
        self.assertFalse(hasattr(slidetest, '_end'), "Present attribute '_end'")
        self.assertFalse(hasattr(slidetest, '_start'), "Present attribute '_start'")
        self.assertFalse(hasattr(slidetest, 'nulmaxrange'), "Present attribute 'nulmaxrange'")
        self.assertFalse(hasattr(slidetest, 'nulminrange'), "Present attribute 'nulminrange'")
        self.assertFalse(hasattr(slidetest, 'get_end'), "Present attribute 'get_end'")
        self.assertFalse(hasattr(slidetest, 'get_start'), "Present attribute 'get_start'")
        self.assertFalse(hasattr(slidetest, 'set_end'), "Present attribute 'set_end'")
        self.assertFalse(hasattr(slidetest, 'set_start'), "Present attribute 'set_start'")
        self.assertEqual(slidetest.get_position(), 30, "Position should be 30")
        slidetest.set_position(50)
        self.assertEqual(slidetest.get_position(), 50, "Position should be 50")

    def test_methods_initialization_with_start(self):
        """Test initialisation methods with warninglevel"""
        class E(metaclass=FactorySlide, start=10):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = E()
        self.assertFalse(hasattr(slidetest, '_position'), "Present attribute '_position'")
        self.assertFalse(hasattr(slidetest, 'get_position'), "Present attribute 'get_position'")
        self.assertFalse(hasattr(slidetest, 'set_position'), "Present attribute 'set_position'")
        self.assertTrue(hasattr(slidetest, '_end'), "Missing attribute '_end'")
        self.assertTrue(hasattr(slidetest, '_start'), "Missing attribute '_start'")
        self.assertTrue(hasattr(slidetest, 'nulmaxrange'), "Missing attribute 'nulmaxrange'")
        self.assertTrue(hasattr(slidetest, 'nulminrange'), "Missing attribute 'nulminrange'")
        self.assertTrue(hasattr(slidetest, 'get_end'), "Missing attribute 'get_end'")
        self.assertTrue(hasattr(slidetest, 'get_start'), "Missing attribute 'get_start'")
        self.assertTrue(hasattr(slidetest, 'set_end'), "Missing attribute 'set_end'")
        self.assertTrue(hasattr(slidetest, 'set_start'), "Missing attribute 'set_start'")
        self.assertIsNone(slidetest.get_end(), "End value should be None")
        self.assertEqual(slidetest.get_start(), 10, "Start should be 10")
        slidetest.set_start(20)
        slidetest.set_end(90)
        self.assertEqual(slidetest.get_end(), 90, "End should be 90")
        self.assertEqual(slidetest.get_start(), 20, "Start should be 20")

    def test_methods_initialization_with_end(self):
        """Test initialisation methods with warninglevel"""
        class F(metaclass=FactorySlide, end=90):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = F()
        self.assertFalse(hasattr(slidetest, '_position'), "Present attribute '_position'")
        self.assertFalse(hasattr(slidetest, 'get_position'), "Present attribute 'get_position'")
        self.assertFalse(hasattr(slidetest, 'set_position'), "Present attribute 'set_position'")
        self.assertTrue(hasattr(slidetest, '_end'), "Missing attribute '_end'")
        self.assertTrue(hasattr(slidetest, '_start'), "Missing attribute '_start'")
        self.assertTrue(hasattr(slidetest, 'nulmaxrange'), "Missing attribute 'nulmaxrange'")
        self.assertTrue(hasattr(slidetest, 'nulminrange'), "Missing attribute 'nulminrange'")
        self.assertTrue(hasattr(slidetest, 'get_end'), "Missing attribute 'get_end'")
        self.assertTrue(hasattr(slidetest, 'get_start'), "Missing attribute 'get_start'")
        self.assertTrue(hasattr(slidetest, 'set_end'), "Missing attribute 'set_end'")
        self.assertTrue(hasattr(slidetest, 'set_start'), "Missing attribute 'set_start'")
        self.assertEqual(slidetest.get_end(), 90, "End should be 90")
        self.assertIsNone(slidetest.get_start(), "Start value should be None")
        slidetest.set_end(80)
        slidetest.set_start(20)
        self.assertEqual(slidetest.get_end(), 80, "End should be 80")
        self.assertEqual(slidetest.get_start(), 20, "Start should be 20")

    def test_methods_initialization_with_start_end(self):
        """Test initialisation methods with warninglevel"""
        class G(metaclass=FactorySlide, start=10, end=90):
            def __init__(self):
                self._maximum = 100
                self._minimum = 0
        slidetest = G()
        self.assertFalse(hasattr(slidetest, '_position'), "Present attribute '_position'")
        self.assertFalse(hasattr(slidetest, 'get_position'), "Present attribute 'get_position'")
        self.assertFalse(hasattr(slidetest, 'set_position'), "Present attribute 'set_position'")
        self.assertTrue(hasattr(slidetest, '_end'), "Missing attribute '_end'")
        self.assertTrue(hasattr(slidetest, '_start'), "Missing attribute '_start'")
        self.assertTrue(hasattr(slidetest, 'nulmaxrange'), "Missing attribute 'nulmaxrange'")
        self.assertTrue(hasattr(slidetest, 'nulminrange'), "Missing attribute 'nulminrange'")
        self.assertTrue(hasattr(slidetest, 'get_end'), "Missing attribute 'get_end'")
        self.assertTrue(hasattr(slidetest, 'get_start'), "Missing attribute 'get_start'")
        self.assertTrue(hasattr(slidetest, 'set_end'), "Missing attribute 'set_end'")
        self.assertTrue(hasattr(slidetest, 'set_start'), "Missing attribute 'set_start'")
        self.assertEqual(slidetest.get_end(), 90, "End should be 90")
        self.assertEqual(slidetest.get_start(), 10, "Start should be 10")
        slidetest.set_end(80)
        slidetest.set_start(20)
        self.assertEqual(slidetest.get_end(), 80, "End should be 80")
        self.assertEqual(slidetest.get_start(), 20, "Start should be 20")


if __name__ == '__main__':
    unittest.main()
