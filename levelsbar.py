"""
    :Date: |date|
    :Revision: 2.0
    :Author: Franc SERRES <sefran007@gmail.com>
    :Description: This is a documention for python 3 bar slide api
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.
    :Release Life Cycle: Release candidate

    The ``levelsbar`` module
    ===========================

    Use it get class progress levels bar features.

        .. image:: classes/classes_levelsbar.png
            :alt: All classes levelsbar.py
            :align: center
            :width: 800px
            :height: 200px

    Example:

    .. code-block:: python

        from levelsbar import QLevelsBar

        min_bar = 1
        max_bar = 100

        myObject = QLevelsBar(min_bar, max_bar, start=1, end=45,
                        alertmin=10, warningmin=30, warningmax=70, alertmax=90)


"""
# pylint: disable=C0302,R0912,R0914,R0915

from progressbar import QBar

class FactoryLevels(type):
    """
    .. image:: classes/FactoryLevels.png
        :alt: Diagram
        :align: center
        :height: 300px

    Create a object with levels informations and functions.

    :param alertmin: Level of minimum alert value.
    :type alertmin: int, float
    :param warningmin: Level of minimum warning value.
    :type warningmin: int, float
    :param warningmax: Level of maximum warning value.
    :type  warningmax: int, float
    :param alertmax: Level of maximum alert value.
    :type alertmax: int, float
    :param setalertlevelmin: Set alert min features.
    :type setalertlevelmin: bool
    :param setwarninglevelmin: Set warning min features.
    :type setwarninglevelmin: bool
    :param setwarninglevelmax: Set warning max features.
    :type setwarninglevelmax: bool
    :param setalertlevelmax: Set  alert max features.
    :type setalertlevelmax: bool
    :param setalertlevels: Set alert features.
    :type setalertlevels: bool
    :param setwarninglevels: Set warning features.
    :type setwarninglevels: bool
    :param setminimumlevels: Set minimum levels features.
    :type setminimumlevels: bool
    :param setmaximumlevels: Set maximum levels features.
    :type setmaximumlevels: bool
    :param setalllevels: Set all levels features.
    :type setalllevels: bool

    Example::

        from rangewidgets import FactoryLevels

        class TestClass(metaclass=FactoryLevels,
                        alertmin=10, warningmin=20, warningmax=80, alertmax=90):
            pass

        rangeobject = TestClass()
        print('Minimum alert end: %s' % rangeobject.get_minimum_alert())
        print('Minimum warning end: %s' % rangeobject.get_minimum_warning())
        print('Maximum warning start: %s' % rangeobject.get_maximum_warning())
        print('Maximum alert start: %s' % rangeobject.get_maximum_alert())
    """
    # pylint: disable=W0212,E1101,W0613,C0204,W0201
    def __new__(cls, name, bases, namespace, **kwargs):
        """ Set class FactoryLevels. """
        setalertlevelmin = False
        setwarninglevelmin = False
        setwarninglevelmax = False
        setalertlevelmax = False
        if 'setalllevels' in kwargs:
            if kwargs['setalllevels']:
                setalertlevelmin = True
                setwarninglevelmin = True
                setwarninglevelmax = True
                setalertlevelmax = True
            del kwargs['setalllevels']
        if 'setminimumlevels' in kwargs:
            if kwargs['setminimumlevels']:
                setalertlevelmin = True
                setwarninglevelmin = True
            del kwargs['setminimumlevels']
        if 'setmaximumlevels' in kwargs:
            if kwargs['setmaximumlevels']:
                setwarninglevelmax = True
                setalertlevelmax = True
            del kwargs['setmaximumlevels']
        if 'setalertlevels' in kwargs:
            if kwargs['setalertlevels']:
                setalertlevelmin = True
                setalertlevelmax = True
            del kwargs['setalertlevels']
        if 'setwarninglevels' in kwargs:
            if kwargs['setwarninglevels']:
                setwarninglevelmin = True
                setwarninglevelmax = True
            del kwargs['setwarninglevels']
        if 'setalertlevelmin' in kwargs:
            if kwargs['setalertlevelmin']:
                setalertlevelmin = True
            del kwargs['setalertlevelmin']
        if 'setwarninglevelmin' in kwargs:
            if kwargs['setwarninglevelmin']:
                setwarninglevelmin = True
            del kwargs['setwarninglevelmin']
        if 'setwarninglevelmax' in kwargs:
            if kwargs['setwarninglevelmax']:
                setwarninglevelmax = True
            del kwargs['setwarninglevelmax']
        if 'setalertlevelmax' in kwargs:
            if kwargs['setalertlevelmax']:
                setalertlevelmax = True
            del kwargs['setalertlevelmax']
        if 'alertmin' in kwargs and kwargs['alertmin'] is not None:
            setalertlevelmin = True
        if 'warningmin' in kwargs and kwargs['warningmin'] is not None:
            setwarninglevelmin = True
        if 'warningmax' in kwargs and kwargs['warningmax'] is not None:
            setwarninglevelmax = True
        if 'alertmax' in kwargs and kwargs['alertmax'] is not None:
            setalertlevelmax = True

        newclass = super(FactoryLevels, cls).__new__(cls, name, bases, namespace)

        if setalertlevelmin:
            # Initialisation parameters
            if 'alertmin' in kwargs and kwargs['alertmin'] is not None:
                setattr(newclass, '_alert_zone_min', kwargs['alertmin'])
                setattr(newclass, '_alert_minimum_show', True)
            else:
                setattr(newclass, '_alert_zone_min', None)
                setattr(newclass, '_alert_minimum_show', False)

            # Initialisation methods
            def set_minimum_alert(self, value):
                """
                Set end minimum alert level value.

                :param value: Minimum end alert level.
                :type value: int, float
                """
                self._alert_zone_min = value

            def get_minimum_alert(self):
                """
                Get end minimum alert level value.

                :returns: Minimum end alert level.
                :rtype: int, float
                """
                return self._alert_zone_min

            def show_minimum_alert(self):
                """
                Show minimum alert.
                """
                self._alert_minimum_show = True

            def hide_minimum_alert(self):
                """
                Hide minimum alert.
                """
                self._alert_minimum_show = False

            def is_minimum_alert_show(self):
                """
                Set of minimum alert show.

                :returns: Set of Minimum alert show
                :rtype: bool
                """
                return self._alert_minimum_show

            setattr(newclass, show_minimum_alert.__name__, show_minimum_alert)
            setattr(newclass, hide_minimum_alert.__name__, hide_minimum_alert)
            setattr(newclass, is_minimum_alert_show.__name__, is_minimum_alert_show)
            setattr(newclass, set_minimum_alert.__name__, set_minimum_alert)
            setattr(newclass, get_minimum_alert.__name__, get_minimum_alert)

        if setwarninglevelmin:
            # Initialisation parameters
            if 'warningmin' in kwargs and kwargs['warningmin'] is not None:
                setattr(newclass, '_warning_zone_min', kwargs['warningmin'])
                setattr(newclass, '_warning_minimum_show', True)
            else:
                setattr(newclass, '_warning_zone_min', None)
                setattr(newclass, '_warning_minimum_show', False)

            # Initialisation methods
            def set_minimum_warning(self, value):
                """
                Set end minimum warning level value.

                :param value: Minimum end warning level.
                :type value: int, float
                """
                self._warning_zone_min = value

            def get_minimum_warning(self):
                """
                Get end warning minimum level value.

                :returns: Minimum end level warning.
                :rtype: int, float
                """
                return self._warning_zone_min

            def show_minimum_warning(self):
                """
                Show minimum warning level.
                """
                self._warning_minimum_show = True

            def hide_minimum_warning(self):
                """
                Hide minimum warning level.
                """
                self._warning_minimum_show = False

            def is_minimum_warning_show(self):
                """
                Set of minimum warning show.

                :returns: Set of Minimum warning show
                :rtype: bool
                """
                return self._warning_minimum_show

            setattr(newclass, set_minimum_warning.__name__, set_minimum_warning)
            setattr(newclass, get_minimum_warning.__name__, get_minimum_warning)
            setattr(newclass, show_minimum_warning.__name__, show_minimum_warning)
            setattr(newclass, hide_minimum_warning.__name__, hide_minimum_warning)
            setattr(newclass, is_minimum_warning_show.__name__, is_minimum_warning_show)

        if setwarninglevelmax:
            # Initialisation parameters
            if 'warningmax' in kwargs and kwargs['warningmax'] is not None:
                setattr(newclass, '_warning_zone_max', kwargs['warningmax'])
                setattr(newclass, '_warning_maximum_show', True)
            else:
                setattr(newclass, '_warning_zone_max', None)
                setattr(newclass, '_warning_maximum_show', False)

            # Initialisation methods
            def set_maximum_warning(self, value):
                """
                Set start maximum warning level value.

                :param value: Maximum start warning level.
                :type value: int, float
                """
                self._warning_zone_max = value

            def get_maximum_warning(self):
                """
                Get start maximum warning level value.

                :returns: Maximum start warning level.
                :rtype: int, float
                """
                return self._warning_zone_max

            def show_maximum_warning(self):
                """
                Show maximum warning level.
                """
                self._warning_maximum_show = True

            def hide_maximum_warning(self):
                """
                Hide maximum warning level.
                """
                self._warning_maximum_show = False

            def is_maximum_warning_show(self):
                """
                Set of maximum warning show.

                :returns: Set of Maximum warning show
                :rtype: bool
                """
                return self._warning_maximum_show

            setattr(newclass, set_maximum_warning.__name__, set_maximum_warning)
            setattr(newclass, get_maximum_warning.__name__, get_maximum_warning)
            setattr(newclass, show_maximum_warning.__name__, show_maximum_warning)
            setattr(newclass, hide_maximum_warning.__name__, hide_maximum_warning)
            setattr(newclass, is_maximum_warning_show.__name__, is_maximum_warning_show)

        if setalertlevelmax:
            # Initialisation parameters
            if 'alertmax' in kwargs and kwargs['alertmax'] is not None:
                setattr(newclass, '_alert_zone_max', kwargs['alertmax'])
                setattr(newclass, '_alert_maximum_show', True)
            else:
                setattr(newclass, '_alert_zone_max', None)
                setattr(newclass, '_alert_maximum_show', False)

            # Initialisation methods
            def set_maximum_alert(self, value):
                """
                Set start maximum alert level value.

                :param value: Maximum start alert level.
                :type value: int, float
                """
                self._alert_zone_max = value

            def get_maximum_alert(self):
                """
                Get start maximum alert level value.

                :returns: Maximum start alert level.
                :rtype: int, float
                """
                return self._alert_zone_max

            def show_maximum_alert(self):
                """
                Show maximum alert
                """
                self._alert_maximum_show = True

            def hide_maximum_alert(self):
                """
                Hide maximum alert
                """
                self._alert_maximum_show = False

            def is_maximum_alert_show(self):
                """
                Set of maximum alert show

                :returns: Set of Maximum alert show
                :rtype: bool
                """
                return self._alert_maximum_show

            setattr(newclass, set_maximum_alert.__name__, set_maximum_alert)
            setattr(newclass, get_maximum_alert.__name__, get_maximum_alert)
            setattr(newclass, show_maximum_alert.__name__, show_maximum_alert)
            setattr(newclass, hide_maximum_alert.__name__, hide_maximum_alert)
            setattr(newclass, is_maximum_alert_show.__name__, is_maximum_alert_show)

        if setalertlevelmin or setalertlevelmax:
            def show_alerts(self):
                """
                Show alerts levels.
                """
                if hasattr(self, "show_minimum_alert"):
                    self.show_minimum_alert()
                if hasattr(self, "show_maximum_alert"):
                    self.show_maximum_alert()

            def hide_alerts(self):
                """
                Hide alerts levels.
                """
                if hasattr(self, "hide_minimum_alert"):
                    self.hide_minimum_alert()
                if hasattr(self, "hide_maximum_alert"):
                    self.hide_maximum_alert()

            def is_alerts_show(self):
                """
                All alerts levels are show.

                :returns: Alerts show
                :rtype: bool
                """
                if hasattr(self, "is_minimum_alert_show") and \
                   hasattr(self, "is_maximum_alert_show"):
                    if self.is_minimum_alert_show() and self.is_maximum_alert_show():
                        return True
                elif hasattr(self, "is_minimum_alert_show"):
                    if self.is_minimum_alert_show():
                        return True
                elif hasattr(self, "is_maximum_alert_show"):
                    if self.is_maximum_alert_show():
                        return True
                return False

            def is_a_alert_show(self):
                """
                A alert level is show.

                :returns: A alert show
                :rtype: bool
                """
                if hasattr(self, "is_minimum_alert_show") and \
                   hasattr(self, "is_maximum_alert_show"):
                    if self.is_minimum_alert_show() or self.is_maximum_alert_show():
                        return True
                elif hasattr(self, "is_minimum_alert_show"):
                    if self.is_minimum_alert_show():
                        return True
                elif hasattr(self, "is_maximum_alert_show"):
                    if self.is_maximum_alert_show():
                        return True
                return False

            setattr(newclass, show_alerts.__name__, show_alerts)
            setattr(newclass, hide_alerts.__name__, hide_alerts)
            setattr(newclass, is_alerts_show.__name__, is_alerts_show)
            setattr(newclass, is_a_alert_show.__name__, is_a_alert_show)

        if setwarninglevelmin or setwarninglevelmax:
            def show_warnings(self):
                """
                Show warnings levels.
                """
                if hasattr(self, "show_minimum_warning"):
                    self.show_minimum_warning()
                if hasattr(self, "show_maximum_warning"):
                    self.show_maximum_warning()
                self._setlevelsshow = True

            def hide_warnings(self):
                """
                Hide warnings levels.
                """
                if hasattr(self, "hide_minimum_warning"):
                    self.hide_minimum_warning()
                if hasattr(self, "hide_maximum_warning"):
                    self.hide_maximum_warning()

            def is_warnings_show(self):
                """
                All warnings levels are show.

                :returns: Warnings show
                :rtype: bool
                """
                if hasattr(self, "is_minimum_warning_show") and \
                   hasattr(self, "is_maximum_warning_show"):
                    if self.is_minimum_warning_show() and self.is_maximum_warning_show():
                        return True
                elif hasattr(self, "is_minimum_warning_show"):
                    if self.is_minimum_warning_show():
                        return True
                elif hasattr(self, "is_maximum_warning_show"):
                    if self.is_maximum_warning_show():
                        return True
                return False

            def is_a_warning_show(self):
                """
                A warning level is show.

                :returns: A warning show
                :rtype: bool

                """
                if hasattr(self, "is_minimum_warning_show") and \
                   hasattr(self, "is_maximum_warning_show"):
                    if self.is_minimum_warning_show() or self.is_maximum_warning_show():
                        return True
                elif hasattr(self, "is_minimum_warning_show"):
                    if self.is_minimum_warning_show():
                        return True
                elif hasattr(self, "is_maximum_warning_show"):
                    if self.is_maximum_warning_show():
                        return True
                return False

            setattr(newclass, show_warnings.__name__, show_warnings)
            setattr(newclass, hide_warnings.__name__, hide_warnings)
            setattr(newclass, is_warnings_show.__name__, is_warnings_show)
            setattr(newclass, is_a_warning_show.__name__, is_a_warning_show)

        if setalertlevelmin or setwarninglevelmin:
            def show_minimums_levels(self):
                """
                Show minimums levels.
                """
                if hasattr(self, "show_minimum_alert"):
                    self.show_minimum_alert()
                if hasattr(self, "show_minimum_warning"):
                    self.show_minimum_warning()

            def hide_minimums_levels(self):
                """
                Hide minimums levels.
                """
                if hasattr(self, "hide_minimum_alert"):
                    self.hide_minimum_alert()
                if hasattr(self, "hide_minimum_warning"):
                    self.hide_minimum_warning()

            def is_minimum_levels_show(self):
                """
                All minimum levels are show.

                :returns: Minimum levels show
                :rtype: bool
                """
                if hasattr(self, "is_minimum_alert_show") and \
                   hasattr(self, "is_minimum_warning_show"):
                    if self.is_minimum_alert_show() and self.is_minimum_warning_show():
                        return True
                elif hasattr(self, "is_minimum_alert_show"):
                    if self.is_minimum_alert_show():
                        return True
                elif hasattr(self, "is_minimum_warning_show"):
                    if self.is_minimum_warning_show():
                        return True
                return False

            def is_a_minimum_level_show(self):
                """
                A minimum level is show.

                :returns: A minimum show
                :rtype: bool
                """
                if hasattr(self, "is_minimum_alert_show") and \
                   hasattr(self, "is_minimum_warning_show"):
                    if self.is_minimum_alert_show() or self.is_minimum_warning_show():
                        return True
                elif hasattr(self, "is_minimum_alert_show"):
                    if self.is_minimum_alert_show():
                        return True
                elif hasattr(self, "is_minimum_warning_show"):
                    if self.is_minimum_warning_show():
                        return True
                return False

            setattr(newclass, show_minimums_levels.__name__, show_minimums_levels)
            setattr(newclass, hide_minimums_levels.__name__, hide_minimums_levels)
            setattr(newclass, is_minimum_levels_show.__name__, is_minimum_levels_show)
            setattr(newclass, is_a_minimum_level_show.__name__, is_a_minimum_level_show)

        if setwarninglevelmax or setalertlevelmax:
            def show_maximums_levels(self):
                """
                Show maximums levels.
                """
                if hasattr(self, "show_maximum_warning"):
                    self.show_maximum_warning()
                if hasattr(self, "show_maximum_alert"):
                    self.show_maximum_alert()

            def hide_maximums_levels(self):
                """
                Hide maximums levels.
                """
                if hasattr(self, "hide_maximum_warning"):
                    self.hide_maximum_warning()
                if hasattr(self, "hide_maximum_alert"):
                    self.hide_maximum_alert()

            def is_maximums_levels_show(self):
                """
                All maximum levels are show.

                :returns: Maximum levels show
                :rtype: bool
                """
                if hasattr(self, "is_maximum_alert_show") and \
                   hasattr(self, "is_maximum_warning_show"):
                    if self.is_maximum_alert_show() and self.is_maximum_warning_show():
                        return True
                elif hasattr(self, "is_maximum_alert_show"):
                    if self.is_maximum_alert_show():
                        return True
                elif hasattr(self, "is_maximum_warning_show"):
                    if self.is_maximum_warning_show():
                        return True
                return False

            def is_a_maximum_level_show(self):
                """
                A maximum level is show.

                :returns: A maximum show
                :rtype: bool
                """
                if hasattr(self, "is_maximum_alert_show") and \
                   hasattr(self, "is_maximum_warning_show"):
                    if self.is_maximum_alert_show() or self.is_maximum_warning_show():
                        return True
                elif hasattr(self, "is_maximum_alert_show"):
                    if self.is_maximum_alert_show():
                        return True
                elif hasattr(self, "is_maximum_warning_show"):
                    if self.is_maximum_warning_show():
                        return True
                return False

            setattr(newclass, show_maximums_levels.__name__, show_maximums_levels)
            setattr(newclass, hide_maximums_levels.__name__, hide_maximums_levels)
            setattr(newclass, is_maximums_levels_show.__name__, is_maximums_levels_show)
            setattr(newclass, is_a_maximum_level_show.__name__, is_a_maximum_level_show)

        if setalertlevelmin or setwarninglevelmin or setwarninglevelmax or setalertlevelmax:
            def show_levels(self):
                """
                Show levels bar indicators.
                """
                if hasattr(self, "show_warnings"):
                    self.show_warnings()
                if hasattr(self, "show_alerts"):
                    self.show_alerts()

            def hide_levels(self):
                """
                Hide levels bar indicators.
                """
                if hasattr(self, "hide_warnings"):
                    self.hide_warnings()
                if hasattr(self, "hide_alerts"):
                    self.hide_alerts()

            def is_levels_show(self):
                """
                All levels are show.

                :returns: All levels show
                :rtype: bool
                """
                state = True
                if hasattr(self, "is_minimum_alert_show"):
                    state = state and self.is_minimum_alert_show()
                if hasattr(self, "is_minimum_warning_show"):
                    state = state and self.is_minimum_warning_show()
                if hasattr(self, "is_maximum_warning_show"):
                    state = state and self.is_maximum_warning_show()
                if hasattr(self, "is_maximum_alert_show"):
                    state = state and self.is_maximum_alert_show()
                return state

            def is_a_level_show(self):
                """
                A level is show.

                :returns: A level show
                :rtype: bool
                """
                if hasattr(self, "is_minimum_alert_show") and self.is_minimum_alert_show():
                    return True
                if hasattr(self, "is_minimum_warning_show") and self.is_minimum_warning_show():
                    return True
                if hasattr(self, "is_maximum_warning_show") and self.is_maximum_warning_show():
                    return True
                if hasattr(self, "is_maximum_alert_show") and self.is_maximum_alert_show():
                    return True
                return False

            setattr(newclass, show_levels.__name__, show_levels)
            setattr(newclass, hide_levels.__name__, hide_levels)
            setattr(newclass, is_levels_show.__name__, is_levels_show)
            setattr(newclass, is_a_level_show.__name__, is_a_level_show)

        return newclass

    def __init__(cls, name, bases, namespace, **kwargs):
        """ Initialize class FactoryLevels. """
        super().__init__(name, bases, namespace)



class QLevelsBar(QBar):
    """
    .. image:: classes/QLevelsBar.png
        :alt: Diagram
        :align: center
        :height: 300px

    Create a levels bar object.

    :param minimum: The minimum bar value.
    :type minimum: int, float
    :param maximum: The maximum bar value.
    :type maximum: int, float
    :param start: The start of range.
    :type start: int, float
    :param end: The end of range.
    :type end: int, float
    :param position: Position of cursor.
    :type position: int, float
    :param start: Start of range.
    :type start: int, float
    :param end: End of range.
    :type end: int, float
    :param setcursor: Set cursor features.
    :type setcursor: bool
    :param setrange: Set range features.
    :type setrange: bool
    :param alertmin: Level of minimum alert value.
    :type alertmin: int, float
    :param warningmin: Level of minimum warning value.
    :type warningmin: int, float
    :param warningmax: Level of maximum warning value.
    :type  warningmax: int, float
    :param alertmax: Level of maximum alert value.
    :type alertmax: int, float
    :param setalertlevelmin: Set alert min features.
    :type setalertlevelmin: bool
    :param setwarninglevelmin: Set warning min features.
    :type setwarninglevelmin: bool
    :param setwarninglevelmax: Set warning max features.
    :type setwarninglevelmax: bool
    :param setalertlevelmax: Set  alert max features.
    :type setalertlevelmax: bool
    :param setalertlevels: Set alert features.
    :type setalertlevels: bool
    :param setwarninglevels: Set warning features.
    :type setwarninglevels: bool
    :param setminimumlevels: Set minimum levels features.
    :type setminimumlevels: bool
    :param setmaximumlevels: Set maximum levels features.
    :type setmaximumlevels: bool
    :param setalllevels: Set all levels features.
    :type setalllevels: bool

    Example::

        from rangewidgets import QLevelsBar

        rangeobject = QLevelsBar(1,100,start=20,end=80)
        print('Minimum: %s' % rangeobject.get_minimum())
        print('Start: %s' % rangeobject.get_start())
        print('End: %s' % rangeobject.get_end())
        print('Maximum: %s' % rangeobject.get_maximum())
    """
    # pylint: disable=W0212,W0235,E1101
    def __new__(cls, *args, **kwargs):
        """ Set class FactoryLevels. """
        setalertlevelmin = False
        setwarninglevelmin = False
        setwarninglevelmax = False
        setalertlevelmax = False
        if 'setalllevels' in kwargs:
            if kwargs['setalllevels']:
                setalertlevelmin = True
                setwarninglevelmin = True
                setwarninglevelmax = True
                setalertlevelmax = True
            del kwargs['setalllevels']
        if 'setminimumlevels' in kwargs:
            if kwargs['setminimumlevels']:
                setalertlevelmin = True
                setwarninglevelmin = True
            del kwargs['setminimumlevels']
        if 'setmaximumlevels' in kwargs:
            if kwargs['setmaximumlevels']:
                setwarninglevelmax = True
                setalertlevelmax = True
            del kwargs['setmaximumlevels']
        if 'setalertlevels' in kwargs:
            if kwargs['setalertlevels']:
                setalertlevelmin = True
                setalertlevelmax = True
            del kwargs['setalertlevels']
        if 'setwarninglevels' in kwargs:
            if kwargs['setwarninglevels']:
                setwarninglevelmin = True
                setwarninglevelmax = True
            del kwargs['setwarninglevels']
        if 'setalertlevelmin' in kwargs:
            if kwargs['setalertlevelmin']:
                setalertlevelmin = True
            del kwargs['setalertlevelmin']
        if 'setwarninglevelmin' in kwargs:
            if kwargs['setwarninglevelmin']:
                setwarninglevelmin = True
            del kwargs['setwarninglevelmin']
        if 'setwarninglevelmax' in kwargs:
            if kwargs['setwarninglevelmax']:
                setwarninglevelmax = True
            del kwargs['setwarninglevelmax']
        if 'setalertlevelmax' in kwargs:
            if kwargs['setalertlevelmax']:
                setalertlevelmax = True
            del kwargs['setalertlevelmax']
        if 'alertmin' in kwargs and kwargs['alertmin'] is not None:
            setalertlevelmin = True
        if 'warningmin' in kwargs and kwargs['warningmin'] is not None:
            setwarninglevelmin = True
        if 'warningmax' in kwargs and kwargs['warningmax'] is not None:
            setwarninglevelmax = True
        if 'alertmax' in kwargs and kwargs['alertmax'] is not None:
            setalertlevelmax = True

        newclass = super(QLevelsBar, cls).__new__(cls, *args, **kwargs)

        if setalertlevelmin:
            # Initialisation parameters
            if 'alertmin' in kwargs and kwargs['alertmin'] is not None:
                setattr(newclass, '_alert_zone_min', kwargs['alertmin'])
                setattr(newclass, '_alert_minimum_show', True)
            else:
                setattr(newclass, '_alert_zone_min', None)
                setattr(newclass, '_alert_minimum_show', False)

            # Initialisation methods
            def set_minimum_alert(value):
                """
                Set end minimum alert level value.

                :param value: Minimum end alert level.
                :type value: int, float
                """
                newclass._alert_zone_min = value

            def get_minimum_alert():
                """
                Get end minimum alert level value.

                :returns: Minimum end alert level.
                :rtype: int, float
                """
                return newclass._alert_zone_min

            def show_minimum_alert():
                """
                Show minimum alert.
                """
                newclass._alert_minimum_show = True

            def hide_minimum_alert():
                """
                Hide minimum alert.
                """
                newclass._alert_minimum_show = False

            def is_minimum_alert_show():
                """
                Set of minimum alert show.

                :returns: Set of Minimum alert show
                :rtype: bool
                """
                return newclass._alert_minimum_show

            setattr(newclass, show_minimum_alert.__name__, show_minimum_alert)
            setattr(newclass, hide_minimum_alert.__name__, hide_minimum_alert)
            setattr(newclass, is_minimum_alert_show.__name__, is_minimum_alert_show)
            setattr(newclass, set_minimum_alert.__name__, set_minimum_alert)
            setattr(newclass, get_minimum_alert.__name__, get_minimum_alert)

        if setwarninglevelmin:
            # Initialisation parameters
            if 'warningmin' in kwargs and kwargs['warningmin'] is not None:
                setattr(newclass, '_warning_zone_min', kwargs['warningmin'])
                setattr(newclass, '_warning_minimum_show', True)
            else:
                setattr(newclass, '_warning_zone_min', None)
                setattr(newclass, '_warning_minimum_show', False)

            # Initialisation methods
            def set_minimum_warning(value):
                """
                Set end minimum warning level value.

                :param value: Minimum end warning level.
                :type value: int, float
                """
                newclass._warning_zone_min = value

            def get_minimum_warning():
                """
                Get end warning minimum level value.

                :returns: Minimum end level warning.
                :rtype: int, float
                """
                return newclass._warning_zone_min

            def show_minimum_warning():
                """
                Show minimum warning level.
                """
                newclass._warning_minimum_show = True

            def hide_minimum_warning():
                """
                Hide minimum warning level.
                """
                newclass._warning_minimum_show = False

            def is_minimum_warning_show():
                """
                Set of minimum warning show.

                :returns: Set of Minimum warning show
                :rtype: bool
                """
                return newclass._warning_minimum_show

            setattr(newclass, set_minimum_warning.__name__, set_minimum_warning)
            setattr(newclass, get_minimum_warning.__name__, get_minimum_warning)
            setattr(newclass, show_minimum_warning.__name__, show_minimum_warning)
            setattr(newclass, hide_minimum_warning.__name__, hide_minimum_warning)
            setattr(newclass, is_minimum_warning_show.__name__, is_minimum_warning_show)

        if setwarninglevelmax:
            # Initialisation parameters
            if 'warningmax' in kwargs and kwargs['warningmax'] is not None:
                setattr(newclass, '_warning_zone_max', kwargs['warningmax'])
                setattr(newclass, '_warning_maximum_show', True)
            else:
                setattr(newclass, '_warning_zone_max', None)
                setattr(newclass, '_warning_maximum_show', False)

            # Initialisation methods
            def set_maximum_warning(value):
                """
                Set start maximum warning level value.

                :param value: Maximum start warning level.
                :type value: int, float
                """
                newclass._warning_zone_max = value

            def get_maximum_warning():
                """
                Get start maximum warning level value.

                :returns: Maximum start warning level.
                :rtype: int, float
                """
                return newclass._warning_zone_max

            def show_maximum_warning():
                """
                Show maximum warning level.
                """
                newclass._warning_maximum_show = True

            def hide_maximum_warning():
                """
                Hide maximum warning level.
                """
                newclass._warning_maximum_show = False

            def is_maximum_warning_show():
                """
                Set of maximum warning show.

                :returns: Set of Maximum warning show
                :rtype: bool
                """
                return newclass._warning_maximum_show

            setattr(newclass, set_maximum_warning.__name__, set_maximum_warning)
            setattr(newclass, get_maximum_warning.__name__, get_maximum_warning)
            setattr(newclass, show_maximum_warning.__name__, show_maximum_warning)
            setattr(newclass, hide_maximum_warning.__name__, hide_maximum_warning)
            setattr(newclass, is_maximum_warning_show.__name__, is_maximum_warning_show)

        if setalertlevelmax:
            # Initialisation parameters
            if 'alertmax' in kwargs and kwargs['alertmax'] is not None:
                setattr(newclass, '_alert_zone_max', kwargs['alertmax'])
                setattr(newclass, '_alert_maximum_show', True)
            else:
                setattr(newclass, '_alert_zone_max', None)
                setattr(newclass, '_alert_maximum_show', False)

            # Initialisation methods
            def set_maximum_alert(value):
                """
                Set start maximum alert level value.

                :param value: Maximum start alert level.
                :type value: int, float
                """
                newclass._alert_zone_max = value

            def get_maximum_alert():
                """
                Get start maximum alert level value.

                :returns: Maximum start alert level.
                :rtype: int, float
                """
                return newclass._alert_zone_max

            def show_maximum_alert():
                """
                Show maximum alert
                """
                newclass._alert_maximum_show = True

            def hide_maximum_alert():
                """
                Hide maximum alert
                """
                newclass._alert_maximum_show = False

            def is_maximum_alert_show():
                """
                Set of maximum alert show

                :returns: Set of Maximum alert show
                :rtype: bool
                """
                return newclass._alert_maximum_show

            setattr(newclass, set_maximum_alert.__name__, set_maximum_alert)
            setattr(newclass, get_maximum_alert.__name__, get_maximum_alert)
            setattr(newclass, show_maximum_alert.__name__, show_maximum_alert)
            setattr(newclass, hide_maximum_alert.__name__, hide_maximum_alert)
            setattr(newclass, is_maximum_alert_show.__name__, is_maximum_alert_show)

        if setalertlevelmin or setalertlevelmax:
            def show_alerts():
                """
                Show alerts levels.
                """
                if hasattr(newclass, "show_minimum_alert"):
                    newclass.show_minimum_alert()
                if hasattr(newclass, "show_maximum_alert"):
                    newclass.show_maximum_alert()

            def hide_alerts():
                """
                Hide alerts levels.
                """
                if hasattr(newclass, "hide_minimum_alert"):
                    newclass.hide_minimum_alert()
                if hasattr(newclass, "hide_maximum_alert"):
                    newclass.hide_maximum_alert()

            def is_alerts_show():
                """
                All alerts levels are show.

                :returns: Alerts show
                :rtype: bool
                """
                if hasattr(newclass, "is_minimum_alert_show") and \
                   hasattr(newclass, "is_maximum_alert_show"):
                    if newclass.is_minimum_alert_show() and newclass.is_maximum_alert_show():
                        return True
                elif hasattr(newclass, "is_minimum_alert_show"):
                    if newclass.is_minimum_alert_show():
                        return True
                elif hasattr(newclass, "is_maximum_alert_show"):
                    if newclass.is_maximum_alert_show():
                        return True
                return False

            def is_a_alert_show():
                """
                A alert level is show.

                :returns: A alert show
                :rtype: bool
                """
                if hasattr(newclass, "is_minimum_alert_show") and \
                   hasattr(newclass, "is_maximum_alert_show"):
                    if newclass.is_minimum_alert_show() or newclass.is_maximum_alert_show():
                        return True
                elif hasattr(newclass, "is_minimum_alert_show"):
                    if newclass.is_minimum_alert_show():
                        return True
                elif hasattr(newclass, "is_maximum_alert_show"):
                    if newclass.is_maximum_alert_show():
                        return True
                return False

            setattr(newclass, show_alerts.__name__, show_alerts)
            setattr(newclass, hide_alerts.__name__, hide_alerts)
            setattr(newclass, is_alerts_show.__name__, is_alerts_show)
            setattr(newclass, is_a_alert_show.__name__, is_a_alert_show)

        if setwarninglevelmin or setwarninglevelmax:
            def show_warnings():
                """
                Show warnings levels.
                """
                if hasattr(newclass, "show_minimum_warning"):
                    newclass.show_minimum_warning()
                if hasattr(newclass, "show_maximum_warning"):
                    newclass.show_maximum_warning()
                newclass._setlevelsshow = True

            def hide_warnings():
                """
                Hide warnings levels.
                """
                if hasattr(newclass, "hide_minimum_warning"):
                    newclass.hide_minimum_warning()
                if hasattr(newclass, "hide_maximum_warning"):
                    newclass.hide_maximum_warning()

            def is_warnings_show():
                """
                All warnings levels are show.

                :returns: Warnings show
                :rtype: bool
                """
                if hasattr(newclass, "is_minimum_warning_show") and \
                   hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_minimum_warning_show() and newclass.is_maximum_warning_show():
                        return True
                elif hasattr(newclass, "is_minimum_warning_show"):
                    if newclass.is_minimum_warning_show():
                        return True
                elif hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_maximum_warning_show():
                        return True
                return False

            def is_a_warning_show():
                """
                A warning level is show.

                :returns: A warning show
                :rtype: bool

                """
                if hasattr(newclass, "is_minimum_warning_show") and \
                   hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_minimum_warning_show() or newclass.is_maximum_warning_show():
                        return True
                elif hasattr(newclass, "is_minimum_warning_show"):
                    if newclass.is_minimum_warning_show():
                        return True
                elif hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_maximum_warning_show():
                        return True
                return False

            setattr(newclass, show_warnings.__name__, show_warnings)
            setattr(newclass, hide_warnings.__name__, hide_warnings)
            setattr(newclass, is_warnings_show.__name__, is_warnings_show)
            setattr(newclass, is_a_warning_show.__name__, is_a_warning_show)

        if setalertlevelmin or setwarninglevelmin:
            def show_minimums_levels():
                """
                Show minimums levels.
                """
                if hasattr(newclass, "show_minimum_alert"):
                    newclass.show_minimum_alert()
                if hasattr(newclass, "show_minimum_warning"):
                    newclass.show_minimum_warning()

            def hide_minimums_levels():
                """
                Hide minimums levels.
                """
                if hasattr(newclass, "hide_minimum_alert"):
                    newclass.hide_minimum_alert()
                if hasattr(newclass, "hide_minimum_warning"):
                    newclass.hide_minimum_warning()

            def is_minimum_levels_show():
                """
                All minimum levels are show.

                :returns: Minimum levels show
                :rtype: bool
                """
                if hasattr(newclass, "is_minimum_alert_show") and \
                   hasattr(newclass, "is_minimum_warning_show"):
                    if newclass.is_minimum_alert_show() and newclass.is_minimum_warning_show():
                        return True
                elif hasattr(newclass, "is_minimum_alert_show"):
                    if newclass.is_minimum_alert_show():
                        return True
                elif hasattr(newclass, "is_minimum_warning_show"):
                    if newclass.is_minimum_warning_show():
                        return True
                return False

            def is_a_minimum_level_show():
                """
                A minimum level is show.

                :returns: A minimum show
                :rtype: bool
                """
                if hasattr(newclass, "is_minimum_alert_show") and \
                   hasattr(newclass, "is_minimum_warning_show"):
                    if newclass.is_minimum_alert_show() or newclass.is_minimum_warning_show():
                        return True
                elif hasattr(newclass, "is_minimum_alert_show"):
                    if newclass.is_minimum_alert_show():
                        return True
                elif hasattr(newclass, "is_minimum_warning_show"):
                    if newclass.is_minimum_warning_show():
                        return True
                return False

            setattr(newclass, show_minimums_levels.__name__, show_minimums_levels)
            setattr(newclass, hide_minimums_levels.__name__, hide_minimums_levels)
            setattr(newclass, is_minimum_levels_show.__name__, is_minimum_levels_show)
            setattr(newclass, is_a_minimum_level_show.__name__, is_a_minimum_level_show)

        if setwarninglevelmax or setalertlevelmax:
            def show_maximums_levels():
                """
                Show maximums levels.
                """
                if hasattr(newclass, "show_maximum_warning"):
                    newclass.show_maximum_warning()
                if hasattr(newclass, "show_maximum_alert"):
                    newclass.show_maximum_alert()

            def hide_maximums_levels():
                """
                Hide maximums levels.
                """
                if hasattr(newclass, "hide_maximum_warning"):
                    newclass.hide_maximum_warning()
                if hasattr(newclass, "hide_maximum_alert"):
                    newclass.hide_maximum_alert()

            def is_maximums_levels_show():
                """
                All maximum levels are show.

                :returns: Maximum levels show
                :rtype: bool
                """
                if hasattr(newclass, "is_maximum_alert_show") and \
                   hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_maximum_alert_show() and newclass.is_maximum_warning_show():
                        return True
                elif hasattr(newclass, "is_maximum_alert_show"):
                    if newclass.is_maximum_alert_show():
                        return True
                elif hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_maximum_warning_show():
                        return True
                return False

            def is_a_maximum_level_show():
                """
                A maximum level is show.

                :returns: A maximum show
                :rtype: bool
                """
                if hasattr(newclass, "is_maximum_alert_show") and \
                   hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_maximum_alert_show() or newclass.is_maximum_warning_show():
                        return True
                elif hasattr(newclass, "is_maximum_alert_show"):
                    if newclass.is_maximum_alert_show():
                        return True
                elif hasattr(newclass, "is_maximum_warning_show"):
                    if newclass.is_maximum_warning_show():
                        return True
                return False

            setattr(newclass, show_maximums_levels.__name__, show_maximums_levels)
            setattr(newclass, hide_maximums_levels.__name__, hide_maximums_levels)
            setattr(newclass, is_maximums_levels_show.__name__, is_maximums_levels_show)
            setattr(newclass, is_a_maximum_level_show.__name__, is_a_maximum_level_show)

        if setalertlevelmin or setwarninglevelmin or setwarninglevelmax or setalertlevelmax:
            def show_levels():
                """
                Show levels bar indicators.
                """
                if hasattr(newclass, "show_warnings"):
                    newclass.show_warnings()
                if hasattr(newclass, "show_alerts"):
                    newclass.show_alerts()

            def hide_levels():
                """
                Hide levels bar indicators.
                """
                if hasattr(newclass, "hide_warnings"):
                    newclass.hide_warnings()
                if hasattr(newclass, "hide_alerts"):
                    newclass.hide_alerts()

            def is_levels_show():
                """
                All levels are show.

                :returns: All levels show
                :rtype: bool
                """
                state = True
                if hasattr(newclass, "is_minimum_alert_show"):
                    state = state and newclass.is_minimum_alert_show()
                if hasattr(newclass, "is_minimum_warning_show"):
                    state = state and newclass.is_minimum_warning_show()
                if hasattr(newclass, "is_maximum_warning_show"):
                    state = state and newclass.is_maximum_warning_show()
                if hasattr(newclass, "is_maximum_alert_show"):
                    state = state and newclass.is_maximum_alert_show()
                return state

            def is_a_level_show():
                """
                A level is show.

                :returns: A level show
                :rtype: bool
                """
                if hasattr(newclass, "is_minimum_alert_show") and \
                   newclass.is_minimum_alert_show():
                    return True
                if hasattr(newclass, "is_minimum_warning_show") and \
                   newclass.is_minimum_warning_show():
                    return True
                if hasattr(newclass, "is_maximum_warning_show") and \
                   newclass.is_maximum_warning_show():
                    return True
                if hasattr(newclass, "is_maximum_alert_show") and \
                   newclass.is_maximum_alert_show():
                    return True
                return False

            setattr(newclass, show_levels.__name__, show_levels)
            setattr(newclass, hide_levels.__name__, hide_levels)
            setattr(newclass, is_levels_show.__name__, is_levels_show)
            setattr(newclass, is_a_level_show.__name__, is_a_level_show)

        return newclass

    def __init__(self, *args, **kwargs):
        """ Initialize class QLevelsBar """
        # Initialize QBar
        super().__init__(*args, **kwargs)
