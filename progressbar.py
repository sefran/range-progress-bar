"""
    .. |date| date::

    :Date: |date|
    :Revision: 2.0
    :Author: Franc SERRES <sefran007@gmail.com>
    :Description: This is a documention for python 3 bar slide api
    :Info: See <https://framagit.org/sefran/range-progress-bar> for more recent info.
    :Release Life Cycle: Release candidate

    .. _formatting-text:

    The ``progressbar`` module
    ===========================

    Use it to import slide bar feature or class slide bar features.

        .. image:: classes/classes_progressbar.png
            :alt: All classes progressbar.py
            :align: center
            :width: 800px
            :height: 200px

    Example:

    .. code-block:: python

        from progressbar import QBar

        start_range = 1
        end_range = 45
        min_range = 1
        max_range = 100

        myObject = QBar(min_range, max_range, start=start_range, end=end_range)

"""

# The short X.Y version. X = major incompatible features, Y = minor added features
__version__ = '2.0'
# The release tag. Tags:
#    'Pre-alpha' = feasibility,
#    'Alpha' = development,
#    'Beta' = testing,
#    'Release candidate' = qualification process,
#    'Stable release' = ready to deploy,
#    'Feature complete' = production ready,
#    'End of life' = obsolete
__release_life_cycle__ = 'Alpha'


class FactorySlide(type):
    """
    .. image:: classes/FactorySlide.png
        :alt: Diagram
        :align: center
        :height: 300px

    Create a object with slide bar features.

    :param position: Position of cursor.
    :type position: int, float
    :param start: Start of range.
    :type start: int, float
    :param end: End of range.
    :type end: int, float
    :param setcursor: Set cursor features.
    :type setcursor: bool
    :param setrange: Set range features.
    :type setrange: bool

    Example::

        from progressbar import FactorySlide

        class Slide(FactorySlide, start=30, end=70)
            ''' Bar class '''
            def __init__():
                ''' Initialize Bar class '''
                self._maximum = 100
                self._minimum = 0
        rangeslide = Slide()
        print('Start of slide: %s' % rangeslide.get_start())
        print('End of slide: %s' % rangeslide.get_end())
    """
    # pylint: disable=W0212,W0613,R0912,R0913,R0915,C0204
    def __new__(cls, name, bases, namespace, **kwargs):
        """ Set class FactorySlide """
        setcursor = False
        setrange = False
        if 'setcursor' in kwargs:
            if kwargs['setcursor']:
                setcursor = True
            del kwargs['setcursor']
        if 'setrange' in kwargs:
            if kwargs['setrange']:
                setrange = True
            del kwargs['setrange']
        if 'position' in kwargs:
            setcursor = True
        if 'start' in kwargs or 'end' in kwargs:
            setrange = True

        newclass = super(FactorySlide, cls).__new__(cls, name, bases, namespace)

        if setcursor:
            if 'position' in kwargs:
                setattr(newclass, '_position', kwargs['position'])
            else:
                setattr(newclass, '_position', None)
            def get_position(self):
                """
                Get position cursor value.

                :returns: Value of position cursor.
                :rtype: int, float
                """
                return self._position
            def set_position(self, value):
                """
                Set start range value.

                :param value: Start range value -- default value 20.
                :type value: int, float
                """
                if self._minimum is not None and value < self._minimum:
                    self._position = self._minimum
                elif self._maximum is not None and value > self._maximum:
                    self._position = self._maximum
                else:
                    self._position = value
            setattr(newclass, get_position.__name__, get_position)
            setattr(newclass, set_position.__name__, set_position)
        if setrange:
            setattr(newclass, 'nulminrange', None)
            setattr(newclass, 'nulmaxrange', None)
            if 'start' in kwargs:
                setattr(newclass, '_start', kwargs['start'])
            else:
                setattr(newclass, '_start', None)
            if 'end' in kwargs:
                setattr(newclass, '_end', kwargs['end'])
            else:
                setattr(newclass, '_end', None)
            def set_start(self, value):
                """
                Set start range value.

                :param value: Start range value -- default value 20.
                :type value: int, float
                """
                if self._minimum is not None and value < self._minimum:
                    self._start = self._minimum
                    self.nulmaxrange = False
                elif self._end is not None and value > self._end:
                    if self._maximum is not None and self._end == self._maximum:
                        self._start = self._maximum
                        self.nulmaxrange = True
                    else:
                        self._start = self._end
                        self.nulmaxrange = False
                else:
                    self._start = value
                    self.nulmaxrange = False
            def get_start(self):
                """
                Get start range value.

                :returns: Value of start range.
                :rtype: int, float
                """
                return self._start
            def set_end(self, value):
                """
                Set end range value.

                :param value: End range value -- default value 80.
                :type value: int, float
                """
                if self._maximum is not None and value > self._maximum:
                    self._end = self._maximum
                    self.nulminrange = False
                elif self._start is not None and value < self._start:
                    if self._minimum is not None and self._start == self._minimum:
                        self._end = self._minimum
                        self.nulminrange = True
                    else:
                        self._end = self._start
                        self.nulminrange = False
                else:
                    self._end = value
                    self.nulminrange = False
            def get_end(self):
                """
                Get end range value.

                :returns: Value of end range.
                :rtype: int, float
                """
                return self._end
            setattr(newclass, get_start.__name__, get_start)
            setattr(newclass, set_start.__name__, set_start)
            setattr(newclass, get_end.__name__, get_end)
            setattr(newclass, set_end.__name__, set_end)

        return newclass

    def __init__(cls, name, bases, namespace, position=None, start=None, end=None, **kwargs):
        ''' Init metaclass FactorySlide '''
        super().__init__(name, bases, namespace)


class QBar():
    """
    .. image:: classes/QBar.png
        :alt: Diagram
        :align: center
        :height: 300px

    Create a position slide bar or range slide bar object.

    :param minimum: The minimum bar value.
    :type minimum: int, float
    :param maximum: The maximum bar value.
    :type maximum: int, float
    :param start: The start of range.
    :type start: int, float
    :param end: The end of range.
    :type end: int, float
    :param position: Position of cursor.
    :type position: int, float
    :param start: Start of range.
    :type start: int, float
    :param end: End of range.
    :type end: int, float
    :param setcursor: Set cursor features.
    :type setcursor: bool
    :param setrange: Set range features.
    :type setrange: bool

    Example::

        from progressbar import QBar

        barobject = QBar(1,100)
        print('Minimum: %s' % rangeobject.get_minimum())
        print('Maximum: %s' % rangeobject.get_maximum())

        barobject = QBar(1,100,10,90)
        print('Minimum: %s' % rangeobject.get_minimum())
        print('Start: %s' % rangeobject.get_start())
        print('End: %s' % rangeobject.get_end())
        print('Maximum: %s' % rangeobject.get_maximum())
    """
    # pylint: disable=w0212,W0613,R0912,R0915,E1101,W0201
    def __new__(cls, *args, **kwargs):
        """ Set class QBar """
        #print('cls: %s' % cls)
        #print('kwargs: %s' % kwargs)
        setcursor = False
        setrange = False

        if 'setcursor' in kwargs:
            if kwargs['setcursor']:
                setcursor = True
            del kwargs['setcursor']
        if 'setrange' in kwargs:
            if kwargs['setrange']:
                setrange = True
            del kwargs['setrange']
        if 'position' in kwargs:
            setcursor = True
        if 'start' in kwargs or 'end' in kwargs:
            setrange = True

        newclass = super(QBar, cls).__new__(cls)

        if setcursor:
            if 'position' in kwargs:
                setattr(newclass, '_position', kwargs['position'])
            else:
                setattr(newclass, '_position', None)
            def get_position():
                """
                Get position cursor value.

                :returns: Value of position cursor.
                :rtype: int, float
                """
                return newclass._position
            def set_position(value):
                """
                Set start range value.

                :param value: Start range value -- default value 20.
                :type value: int, float
                """
                if value < newclass._minimum:
                    newclass._position = newclass._minimum
                elif value > newclass._maximum:
                    newclass._position = newclass._maximum
                else:
                    newclass._position = value
            setattr(newclass, get_position.__name__, get_position)
            setattr(newclass, set_position.__name__, set_position)
        if setrange:
            setattr(newclass, 'nulminrange', None)
            setattr(newclass, 'nulmaxrange', None)
            if 'start' in kwargs:
                setattr(newclass, '_start', kwargs['start'])
            else:
                setattr(newclass, '_start', None)
            if 'end' in kwargs:
                setattr(newclass, '_end', kwargs['end'])
            else:
                setattr(newclass, '_end', None)
            def set_start(value):
                """
                Set start range value.

                :param value: Start range value -- default value 20.
                :type value: int, float
                """
                if newclass._minimum is not None and value < newclass._minimum:
                    newclass._start = newclass._minimum
                    newclass.nulmaxrange = False
                elif newclass._end is not None and value > newclass._end:
                    if newclass._maximum is not None and newclass._end == newclass._maximum:
                        newclass._start = newclass._maximum
                        newclass.nulmaxrange = True
                    else:
                        newclass._start = newclass._end
                        newclass.nulmaxrange = False
                else:
                    newclass._start = value
                    newclass.nulmaxrange = False
            def get_start():
                """
                Get start range value.

                :returns: Value of start range.
                :rtype: int, float
                """
                return newclass._start
            def set_end(value):
                """
                Set end range value.
                :param value: End range value -- default value 80.
                :type value: int, float
                """
                if newclass._maximum is not None and value > newclass._maximum:
                    newclass._end = newclass._maximum
                    newclass.nulminrange = False
                elif newclass._start is not None and value < newclass._start:
                    if newclass._minimum is not None and newclass._start == newclass._minimum:
                        newclass._end = newclass._minimum
                        newclass.nulminrange = True
                    else:
                        newclass._end = newclass._start
                        newclass.nulminrange = False
                else:
                    newclass._end = value
                    newclass.nulminrange = False
            def get_end():
                """
                Get end range value.

                :returns: Value of end range.
                :rtype: int, float
                """
                return newclass._end
            setattr(newclass, get_start.__name__, get_start)
            setattr(newclass, set_start.__name__, set_start)
            setattr(newclass, get_end.__name__, get_end)
            setattr(newclass, set_end.__name__, set_end)

        return newclass

    def __init__(self, minimum, maximum, **kwargs):
        """ Initialize class QBar """
        # Initialisation parameters
        self._minimum = minimum
        self._maximum = maximum

    def set_minimum(self, value):
        """
        Set minimum bar value.

        :param value: Minimum bar value -- default value 0.
        :type value: int, float
        """
        self._minimum = value

    def get_minimum(self):
        """
        Get minimum bar value.

        :returns: Value of minimum bar.
        :rtype: int, float
        """
        return self._minimum

    def set_maximum(self, value):
        """
        Set maximum bar value

        :param value: Maximum bar value -- default value 100.
        :type value: int, float
        """
        self._maximum = value

    def get_maximum(self):
        """
        Get maximum bar value.

        :returns: Value of maximum bar.
        :rtype: int, float
        """
        return self._maximum
